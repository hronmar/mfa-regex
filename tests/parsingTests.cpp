#include "memory_automaton.h"
#include "mfa_builder.h"
#include "syntax_tree.h"
#include "exceptions.h"

#include <catch2/catch.hpp>
#include <memory>

using namespace mfa;

TEST_CASE("parsing basic regex expressions", "[parsing]") {
    Parameters params;
    params.m_EnablePatternOptimizations = false;
    MFA_Builder mfaBuilder;
    std::unique_ptr<MemoryAutomaton> automaton;

    SECTION("concatenation") {
        REQUIRE_NOTHROW(automaton = mfaBuilder.build(".a", params));
        SyntaxTreeNode * root = automaton->getSyntaxTree();
        REQUIRE(root->m_NodeType == SyntaxTreeNode::CONCAT);
        REQUIRE(root->m_Left != nullptr);
        REQUIRE(root->m_Left->m_NodeType == SyntaxTreeNode::DOT);
        REQUIRE(root->m_Right != nullptr);
        REQUIRE(root->m_Right->m_NodeType == SyntaxTreeNode::ATOM);
    }

    SECTION("alternation") {
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("a|b|cd", params));
        SyntaxTreeNode * root = automaton->getSyntaxTree();
        REQUIRE(root->m_NodeType == SyntaxTreeNode::ALTER);
        REQUIRE(root->m_Left != nullptr);
        REQUIRE(root->m_Left->m_NodeType == SyntaxTreeNode::ALTER);
        REQUIRE(root->m_Left->m_Left != nullptr);
        REQUIRE(root->m_Left->m_Left->m_NodeType == SyntaxTreeNode::ATOM);
        REQUIRE(root->m_Left->m_Right != nullptr);
        REQUIRE(root->m_Left->m_Right->m_NodeType == SyntaxTreeNode::ATOM);
        REQUIRE(root->m_Left->m_Left != nullptr);
        REQUIRE(root->m_Right != nullptr);
        REQUIRE(root->m_Right->m_NodeType == SyntaxTreeNode::CONCAT);
    }

    SECTION("iteration") {
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("a+", params));
        SyntaxTreeNode * root = automaton->getSyntaxTree();
        REQUIRE(root->m_NodeType == SyntaxTreeNode::POS_ITER);
    }
}

TEST_CASE("parsing back-references and capturing parentheses", "[parsing]") {
    MFA_Builder mfaBuilder;
    std::unique_ptr<MemoryAutomaton> automaton;
    Parameters params;
    params.m_UnusedMemoriesOptEn = false;
    params.m_EnablePatternOptimizations = false;

    SECTION("syntax tree structure") {
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(.*)\\1", params));
        SyntaxTreeNode * root = automaton->getSyntaxTree();
        REQUIRE(root->m_NodeType == SyntaxTreeNode::CONCAT);
        REQUIRE(root->m_Left != nullptr);
        REQUIRE(root->m_Left->m_NodeType == SyntaxTreeNode::CAPTURE);
        REQUIRE(root->m_Right != nullptr);
        REQUIRE(root->m_Right->m_NodeType == SyntaxTreeNode::BACKREF);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("((a+))", params));
        root = automaton->getSyntaxTree();
        REQUIRE(root->m_NodeType == SyntaxTreeNode::CAPTURE);
        REQUIRE(root->m_Left != nullptr);
        REQUIRE(root->m_Right == nullptr);
        REQUIRE(root->m_Left->m_NodeType == SyntaxTreeNode::CAPTURE);
        REQUIRE(root->m_Left->m_Left != nullptr);
        REQUIRE(root->m_Left->m_Right == nullptr);
        REQUIRE(root->m_Left->m_Left->m_NodeType == SyntaxTreeNode::POS_ITER);
    }

    SECTION("capture groups count") {
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("", params));
        REQUIRE(automaton->getNumMemories() == 0);
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("abc(.*)d(.(e.*)f)g.(h)", params));
        REQUIRE(automaton->getNumMemories() == 4);
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:x)", params));
        REQUIRE(automaton->getNumMemories() == 0);
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:(?:x)+)(?:y)", params));
        REQUIRE(automaton->getNumMemories() == 0);
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("((?:x)+)(?:y)", params));
        REQUIRE(automaton->getNumMemories() == 1);
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?|(a)|(b))", params));
        REQUIRE(automaton->getNumMemories() == 1);
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?|(a(b)(c(?:d)))|(d(e)))", params));
        REQUIRE(automaton->getNumMemories() == 3);
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?|(a(?|(b)|(?:c(d))))|(e(f)))", params));
        REQUIRE(automaton->getNumMemories() == 2);
    }
}

TEST_CASE("pattern optimizations", "[parsing][optimization]") {
    Parameters params;
    params.m_EnablePatternOptimizations = true;
    MFA_Builder mfaBuilder;
    std::unique_ptr<MemoryAutomaton> automaton;

    SECTION("alternation") {
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("a|b|c", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::CHAR_SET);
        CharacterSetNode * node = static_cast<CharacterSetNode *>(automaton->getSyntaxTree());
        REQUIRE(node->m_CharacterSet.contains('a'));
        REQUIRE(node->m_CharacterSet.contains('b'));
        REQUIRE(node->m_CharacterSet.contains('c'));

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("a|.", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::DOT);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("[a]|[b]|c", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::CHAR_SET);
        node = static_cast<CharacterSetNode *>(automaton->getSyntaxTree());
        REQUIRE(node->m_CharacterSet.contains('a'));
        REQUIRE(node->m_CharacterSet.contains('b'));
        REQUIRE(node->m_CharacterSet.contains('c'));
    }

    SECTION("quantifiers") {
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:a*)*", params));
        SyntaxTreeNode * root = automaton->getSyntaxTree();
        REQUIRE(root->m_NodeType == SyntaxTreeNode::ITER);
        REQUIRE(root->m_Left->m_NodeType == SyntaxTreeNode::ATOM);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:a+)*", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::ITER);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:a?)*", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::ITER);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:a*)+", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::ITER);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:a+)+", params));
        root = automaton->getSyntaxTree();
        REQUIRE(root->m_NodeType == SyntaxTreeNode::POS_ITER);
        REQUIRE(root->m_Left->m_NodeType == SyntaxTreeNode::ATOM);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:a?)+", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::ITER);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:a*)?", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::ITER);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:a+)?", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::ITER);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:a?)?", params));
        root = automaton->getSyntaxTree();
        REQUIRE(root->m_NodeType == SyntaxTreeNode::OPTIONAL);
        REQUIRE(root->m_Left->m_NodeType == SyntaxTreeNode::ATOM);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:a+?)??", params));
        root = automaton->getSyntaxTree();
        REQUIRE(root->m_NodeType == SyntaxTreeNode::ITER);
        REQUIRE(static_cast<QuantifierNode *>(root)->m_Greedy == false);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:a*?)+", params));
        root = automaton->getSyntaxTree();
        REQUIRE(root->m_NodeType == SyntaxTreeNode::POS_ITER);
        REQUIRE(root->m_Left->m_NodeType == SyntaxTreeNode::ITER);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:a+)??", params));
        root = automaton->getSyntaxTree();
        REQUIRE(root->m_NodeType == SyntaxTreeNode::OPTIONAL);
        REQUIRE(root->m_Left->m_NodeType == SyntaxTreeNode::POS_ITER);
    }

    SECTION("counters") {
        REQUIRE_NOTHROW(automaton = mfaBuilder.build("a{0}", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::EPSILON);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("a{1}", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::ATOM);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("a{0,}", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::ITER);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("a{1,}", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::POS_ITER);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("a{0,1}", params));
        REQUIRE(automaton->getSyntaxTree()->m_NodeType == SyntaxTreeNode::OPTIONAL);

        REQUIRE_NOTHROW(automaton = mfaBuilder.build("(?:a{4}){7}", params));
        SyntaxTreeNode * root = automaton->getSyntaxTree();
        REQUIRE(root->m_NodeType == SyntaxTreeNode::COUNTER);
        REQUIRE(static_cast<CounterNode *>(root)->m_CountingCn.m_Min == 28);
        REQUIRE(static_cast<CounterNode *>(root)->m_CountingCn.m_Max == 28);
        REQUIRE(root->m_Left->m_NodeType == SyntaxTreeNode::ATOM);
    }
}

TEST_CASE("regex expressions syntax checking", "[parsing][exceptions]") {
    MFA_Builder mfaBuilder;
    REQUIRE_NOTHROW(mfaBuilder.build("(a)"));
    REQUIRE_THROWS_AS(mfaBuilder.build("(a"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("((.*)+)?"));
    REQUIRE_THROWS_AS(mfaBuilder.build("(((.*)*)*"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("((.*)*))*"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("\\\\"));
    REQUIRE_THROWS_AS(mfaBuilder.build("\\"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("\\(a\\)"));
    REQUIRE_THROWS_AS(mfaBuilder.build("\\(a\\\\)"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build(".(*)"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build(".(+)"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build(".(?)"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("*"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("+"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("a+?"));
    REQUIRE_NOTHROW(mfaBuilder.build("a*?"));
    REQUIRE_NOTHROW(mfaBuilder.build("a??"));
    REQUIRE_THROWS_AS(mfaBuilder.build("a**"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("a++"), regex_syntax_error);
}

TEST_CASE("regex with backreferences syntax checking", "[parsing][exceptions]") {
    MFA_Builder mfaBuilder;
    REQUIRE_NOTHROW(mfaBuilder.build("a(b)\\1"));
    REQUIRE_THROWS_AS(mfaBuilder.build("ab\\1"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("(a(b)c\\2)"));
    REQUIRE_THROWS_AS(mfaBuilder.build("(a\\1)"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("(a(b)c\\1)+"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("(a(b\\1)*c)+"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("(a(b*)c\\2)+\\1"));
    REQUIRE_NOTHROW(mfaBuilder.build("(\\2(.+)a)*\\1"));
    REQUIRE_NOTHROW(mfaBuilder.build("(?|(a)|(b)|(c))\\1"));
    REQUIRE_THROWS_AS(mfaBuilder.build("(?|(a)|(b)|(c))\\2"), regex_syntax_error);
}

TEST_CASE("regex with named capture groups syntax checking", "[parsing][exceptions]") {
    MFA_Builder mfaBuilder;
    REQUIRE_NOTHROW(mfaBuilder.build("(?<variable>a+)\\k<variable>"));
    REQUIRE_NOTHROW(mfaBuilder.build("(?<x1>abc)\\k<x1>"));
    REQUIRE_THROWS_AS(mfaBuilder.build("(?<1x>a)"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("(?<x-1>a)"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("(?<>a)"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("(?<x>a)b(?<x>c)"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("(?:\\k<x1>(?<x1>abc))+\\1"));
    REQUIRE_THROWS_AS(mfaBuilder.build("\\k<x>"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("(?<xa)"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("(?<x>a*)b+\\1"));
    REQUIRE_THROWS_AS(mfaBuilder.build("(?<x>a*)(?<x>b+)\\k<x>"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("(?<&x>a*b+)\\k<x>"));
    REQUIRE_NOTHROW(mfaBuilder.build("(?<x>a*)(?<&x>b+)\\k<x>"));
    REQUIRE_THROWS_AS(mfaBuilder.build("(?<x>a*)(?<&x>b+)\\2"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("(?|(?<x>a)|(?<x>b))\\k<x>"));
    REQUIRE_THROWS_AS(mfaBuilder.build("(?|(?<x>a)|(?<y>b))\\k<x>\\k<y>"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("(?|(?<x>(?|(?<y>a)|(?<y>b)))|(?<x>c))\\k<x>"));
    REQUIRE_THROWS_AS(mfaBuilder.build("(?|(?<x>(?|(?<y>a)|(?<z>b))|(?<x>c))\\k<x>"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("(?|(?<&x>a)|(?<&x>b))"));
    REQUIRE_THROWS_AS(mfaBuilder.build("(?|(?<&x>a)|(?<&y>b))"), regex_syntax_error);
}

TEST_CASE("regex bracket-expression syntax checking", "[parsing][exceptions]") {
    MFA_Builder mfaBuilder;
    REQUIRE_NOTHROW(mfaBuilder.build("[abc]"));
    REQUIRE_THROWS_AS(mfaBuilder.build("[ab"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("[a-b-c]"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("[[:alnum:]]"));
    REQUIRE_THROWS_AS(mfaBuilder.build("[[:alnum"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("[[:alnum]"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("[[:yyyy:]]"), regex_syntax_error);
    REQUIRE_NOTHROW(mfaBuilder.build("[[:alnum:]-]"));
    REQUIRE_THROWS_AS(mfaBuilder.build("[[:alnum:]-x]"), regex_syntax_error);
}

TEST_CASE("regex counting constraints syntax checking", "[parsing][exceptions]") {
    MFA_Builder mfaBuilder;
    REQUIRE_NOTHROW(mfaBuilder.build("a{4}"));
    REQUIRE_NOTHROW(mfaBuilder.build("a{1,2}"));
    REQUIRE_NOTHROW(mfaBuilder.build("a{3,}"));
    REQUIRE_NOTHROW(mfaBuilder.build("a{3,}?"));
    REQUIRE_THROWS_AS(mfaBuilder.build("a{}"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("a{{1,2}}"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("a{,}"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("a{,5}"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("a{4,xxx}"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("{1,2}"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("{-3}"), regex_syntax_error);
    REQUIRE_THROWS_AS(mfaBuilder.build("{1,-4}"), regex_syntax_error);
}
