#include "avd_optimized_mfa.h"
#include "memory_automaton.h"
#include "mfa_builder.h"
#include "backtrack_matcher.h"
#include "bfs_matcher.h"

#include <catch2/catch.hpp>
#include <memory>
#include <type_traits>

using namespace mfa;

TEMPLATE_PRODUCT_TEST_CASE("matching regex expressions", "[matching][avd][searching]", (BacktrackMatcher, BFSMatcher), (MemoryAutomaton, AvdOptimizedMFA)) {
    Parameters params;
    using Automaton = typename TestType::automaton_t;
    std::unique_ptr<Automaton> automaton;
    TestType matcher;

    /* try all parameters possible values */
    params.m_UnusedMemoriesOptEn = GENERATE(false, true);
    params.m_EnablePatternOptimizations = GENERATE(false, true);

    SECTION("basic regex expressions") {
        SECTION("concatenation") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("ab.c", params));
            REQUIRE(matcher.match(automaton.get(), "abbc"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abcd"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abc"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("..x", params));
            REQUIRE(matcher.match(automaton.get(), "abx"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "axb"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "x"));
        }

        SECTION("alternation") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("a|bc", params));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "bc"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abc"));
        }

        SECTION("iteration") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("a*b", params));
            REQUIRE(matcher.match(automaton.get(), "b"));
            REQUIRE(matcher.match(automaton.get(), "ab"));
            REQUIRE(matcher.match(automaton.get(), "aaab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "a"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>(".*aa.*bb.*", params));
            REQUIRE(matcher.match(automaton.get(), "aabb"));
            REQUIRE(matcher.match(automaton.get(), "abbbaaabbbabaaab"));
            REQUIRE(matcher.match(automaton.get(), "aabababababababb"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaabababab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "bbaa"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?:(?:(?:a*)*)*)b", params));
            REQUIRE(matcher.match(automaton.get(), "b"));
            REQUIRE(matcher.match(automaton.get(), "aaaaaab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaaaaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
        }

        SECTION("positive iteration") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("a+", params));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "aaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?:a+)+", params));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "aaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
        }

        SECTION("optional") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("a?b", params));
            REQUIRE(matcher.match(automaton.get(), "b"));
            REQUIRE(matcher.match(automaton.get(), "ab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?:a|b?c)?x", params));
            REQUIRE(matcher.match(automaton.get(), "x"));
            REQUIRE(matcher.match(automaton.get(), "ax"));
            REQUIRE(matcher.match(automaton.get(), "bcx"));
            REQUIRE(matcher.match(automaton.get(), "cx"));
        }

        SECTION("combinations") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("1(?:1|0)*0|0", params));
            REQUIRE(matcher.match(automaton.get(), "0"));
            REQUIRE(matcher.match(automaton.get(), "10"));
            REQUIRE(matcher.match(automaton.get(), "10110"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "1"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "00"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "010"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "10111"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?:a+b)?|x*y", params));
            REQUIRE(matcher.match(automaton.get(), ""));
            REQUIRE(matcher.match(automaton.get(), "y"));
            REQUIRE(matcher.match(automaton.get(), "xxxy"));
            REQUIRE(matcher.match(automaton.get(), "aaaab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "b"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "a"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "x"));
        }
    }
    
    SECTION("regex with bracket-expressions") {
        SECTION("basic") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[af]"));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "f"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "b"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "0"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[^af]"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "a"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "f"));
            REQUIRE(matcher.match(automaton.get(), "b"));
            REQUIRE(matcher.match(automaton.get(), "0"));
        }

        SECTION("edge cases") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[]]"));
            REQUIRE(matcher.match(automaton.get(), "]"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "["));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[^]]"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "]"));
            REQUIRE(matcher.match(automaton.get(), "["));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[[]"));
            REQUIRE(matcher.match(automaton.get(), "["));
            REQUIRE_FALSE(matcher.match(automaton.get(), "]"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[^.]"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "."));
            REQUIRE(matcher.match(automaton.get(), "a"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[a^]"));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "^"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "b"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[^^]"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "^"));
            REQUIRE(matcher.match(automaton.get(), "a"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[-]"));
            REQUIRE(matcher.match(automaton.get(), "-"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[^-]"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "-"));
        }

        SECTION("character ranges") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[a-fx]"));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "b"));
            REQUIRE(matcher.match(automaton.get(), "d"));
            REQUIRE(matcher.match(automaton.get(), "f"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "h"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "0"));
            REQUIRE(matcher.match(automaton.get(), "x"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[a-]"));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "-"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "b"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[---x]"));
            REQUIRE(matcher.match(automaton.get(), "-"));
            REQUIRE(matcher.match(automaton.get(), "x"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "a"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[----]"));
            REQUIRE(matcher.match(automaton.get(), "-"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "["));
            REQUIRE_FALSE(matcher.match(automaton.get(), "]"));
        }

        SECTION("character classes") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[[:lower:]]"));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "z"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "A"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[[:alnum:]]"));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "A"));
            REQUIRE(matcher.match(automaton.get(), "4"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "*"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("[[:space:][:digit:]-]"));
            REQUIRE(matcher.match(automaton.get(), " "));
            REQUIRE(matcher.match(automaton.get(), "4"));
            REQUIRE(matcher.match(automaton.get(), "-"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "a"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "*"));
        }
    }
    
    SECTION("regex with counting constraints") {
        SECTION("basic") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("a{5}"));
            REQUIRE(matcher.match(automaton.get(), "aaaaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "a"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaaaaa"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("ab{2,4}"));
            REQUIRE(matcher.match(automaton.get(), "abb"));
            REQUIRE(matcher.match(automaton.get(), "abbb"));
            REQUIRE(matcher.match(automaton.get(), "abbbb"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "a"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "ab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abbbbb"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("aa{3,}"));
            REQUIRE(matcher.match(automaton.get(), "aaaa"));
            REQUIRE(matcher.match(automaton.get(), "aaaaa"));
            REQUIRE(matcher.match(automaton.get(), "aaaaaaaaaa"));
            REQUIRE(matcher.match(automaton.get(), "aaaaaaaaaaaaaaaaaaaaaaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "a"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaa"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("a{0,2}"));
            REQUIRE(matcher.match(automaton.get(), ""));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "aa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaa"));
            
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?:a|b){1,3}"));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "aba"));
            REQUIRE(matcher.match(automaton.get(), "bab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaaa"));
            
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?:(a+)b\\1){2,3}"));
            REQUIRE(matcher.match(automaton.get(), "abaaba"));
            REQUIRE(matcher.match(automaton.get(), "aabaaabaaaabaaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abaaaba"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abab"));
            
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("a{0,}"));
            REQUIRE(matcher.match(automaton.get(), ""));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "aaaaaaaaaaaaaaaaaaaaaaa"));
            
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("a{1,}"));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "aa"));
            REQUIRE(matcher.match(automaton.get(), "aaaaaaaaaaaaaaaaaaaaaaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("a{1}"));
            REQUIRE(matcher.match(automaton.get(), "a"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
        }
        
        SECTION("multiple counters") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("a{0,2}b{1,4}"));
            REQUIRE(matcher.match(automaton.get(), "b"));
            REQUIRE(matcher.match(automaton.get(), "ab"));
            REQUIRE(matcher.match(automaton.get(), "abbbb"));
            REQUIRE(matcher.match(automaton.get(), "aabbbb"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aabbbbb"));
            
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?:(?:a{1}){2}){3}"));
            REQUIRE(matcher.match(automaton.get(), "aaaaaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaaaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaaaaaa"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?:a{1,2}){2,4}b{2}"));
            REQUIRE(matcher.match(automaton.get(), "aabb"));
            REQUIRE(matcher.match(automaton.get(), "aaabb"));
            REQUIRE(matcher.match(automaton.get(), "aaaabb"));
            REQUIRE(matcher.match(automaton.get(), "aaaaaaaabb"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abb"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaaaaaaaabb"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaaabbb"));
            
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?:a{0,2}c{3}|(?:a+b{2,}c{0,2})){1,3}d{2}"));            
            REQUIRE(matcher.match(automaton.get(), "aacccdd"));
            REQUIRE(matcher.match(automaton.get(), "abbdd"));
            REQUIRE(matcher.match(automaton.get(), "aabbbbccdd"));
            REQUIRE(matcher.match(automaton.get(), "abbabbabbdd"));
            REQUIRE(matcher.match(automaton.get(), "abbcccabbdd"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "dd"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaacccdd"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abbabbabbabbdd"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abbcccabbabbdd"));
        }
    }
    
    SECTION("regex escape sequences") {
        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("\\w+\\a", params));
        REQUIRE(matcher.match(automaton.get(), "a\a"));
        REQUIRE(matcher.match(automaton.get(), "abc_d\a"));
        REQUIRE(matcher.match(automaton.get(), "abc4d\a"));
        REQUIRE_FALSE(matcher.match(automaton.get(), "abc_d"));
        REQUIRE_FALSE(matcher.match(automaton.get(), "abc \a"));

        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("\\d+\\D\\u\\L\\s*\\n", params));
        REQUIRE(matcher.match(automaton.get(), "123xA_   \n"));
        REQUIRE(matcher.match(automaton.get(), "4-AB \n"));
        REQUIRE(matcher.match(automaton.get(), "1_A_\n"));
        REQUIRE_FALSE(matcher.match(automaton.get(), "4-aB \n"));
        REQUIRE_FALSE(matcher.match(automaton.get(), "4-Ab \n"));
        REQUIRE_FALSE(matcher.match(automaton.get(), "4AB\n"));
        REQUIRE_FALSE(matcher.match(automaton.get(), "4-AB "));
        
        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("\\f\\r\\t\\W", params));
        REQUIRE(matcher.match(automaton.get(), "\f\r\t\n"));
        REQUIRE_FALSE(matcher.match(automaton.get(), "\f\r\tx"));
    }

    SECTION("regex word boundary asserions") {
        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>(".\\b.", params));
        REQUIRE(matcher.match(automaton.get(), "a-"));
        REQUIRE(matcher.match(automaton.get(), " B"));
        REQUIRE_FALSE(matcher.match(automaton.get(), "ab"));
        REQUIRE_FALSE(matcher.match(automaton.get(), "a_"));
        REQUIRE_FALSE(matcher.match(automaton.get(), "**"));

        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>(".*\\b", params));
        REQUIRE(matcher.match(automaton.get(), "abc"));
        REQUIRE(matcher.match(automaton.get(), "a_"));
        REQUIRE_FALSE(matcher.match(automaton.get(), ""));
        REQUIRE_FALSE(matcher.match(automaton.get(), " "));
        REQUIRE_FALSE(matcher.match(automaton.get(), "abc*"));

        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("\\b.*", params));
        REQUIRE(matcher.match(automaton.get(), "abc"));
        REQUIRE(matcher.match(automaton.get(), "a*"));
        REQUIRE_FALSE(matcher.match(automaton.get(), ""));
        REQUIRE_FALSE(matcher.match(automaton.get(), " "));
        REQUIRE_FALSE(matcher.match(automaton.get(), "*a"));
        
        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>(".\\B.", params));
        REQUIRE(matcher.match(automaton.get(), "ab"));
        REQUIRE(matcher.match(automaton.get(), "a_"));
        REQUIRE(matcher.match(automaton.get(), "**"));
        REQUIRE_FALSE(matcher.match(automaton.get(), "a-"));
        REQUIRE_FALSE(matcher.match(automaton.get(), " B"));
    }

    SECTION("regex with back-references") {
        SECTION("basic") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(aa)\\1", params));
            REQUIRE(matcher.match(automaton.get(), "aaaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaa"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(a+)\\1", params));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "a"));
            REQUIRE(matcher.match(automaton.get(), "aa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaa"));
            REQUIRE(matcher.match(automaton.get(), "aaaa"));
            REQUIRE(matcher.match(automaton.get(), "aaaaaa"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(.*)\\1", params));
            REQUIRE(matcher.match(automaton.get(), ""));
            REQUIRE(matcher.match(automaton.get(), "abcabc"));
            REQUIRE(matcher.match(automaton.get(), "abababab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "a"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abcab"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("\\1(aa)", params));
            REQUIRE(matcher.match(automaton.get(), "aa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaaaaa"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(aa)\\1?", params));
            REQUIRE(matcher.match(automaton.get(), "aaaa"));
            REQUIRE(matcher.match(automaton.get(), "aa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaaaaa"));
        }

        SECTION("multiple capture groups") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(.*)x(.*)\\1y\\2", params));
            REQUIRE(matcher.match(automaton.get(), "xy"));
            REQUIRE(matcher.match(automaton.get(), "aaxbbaaybb"));
            REQUIRE(matcher.match(automaton.get(), "xxxxxxxyxx"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaxbbbbyaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "xxxxxxxyx"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(.*)-(.*)-(.*)\\1\\2\\3", params));
            REQUIRE(matcher.match(automaton.get(), "--"));
            REQUIRE(matcher.match(automaton.get(), "a-b-cabc"));
            REQUIRE(matcher.match(automaton.get(), "a--a"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "-x-ax"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "a-b-cab"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(.*)(.)(.*)\\3\\2\\1", params));
            REQUIRE(matcher.match(automaton.get(), "bb"));
            REQUIRE(matcher.match(automaton.get(), "abccba"));
            REQUIRE(matcher.match(automaton.get(), "abcbca"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "bbb"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abcdabcd"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("\\1(x)(y)\\2", params));
            REQUIRE(matcher.match(automaton.get(), "xyy"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "xy"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "xxy"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "xxyy"));
        }

        SECTION("nested") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("((a)b)\\1\\2", params));
            REQUIRE(matcher.match(automaton.get(), "ababa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abaab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aba"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(([ab]+)\\2)\\1", params));
            REQUIRE(matcher.match(automaton.get(), "aaaa"));
            REQUIRE(matcher.match(automaton.get(), "aabaabaabaab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aababaabab"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(\\2(.+)x)*\\1", params));
            REQUIRE(matcher.match(automaton.get(), ""));
            REQUIRE(matcher.match(automaton.get(), "xxxx"));
            REQUIRE(matcher.match(automaton.get(), "axaaxaxaax"));
            REQUIRE(matcher.match(automaton.get(), "aaxaabbxbbxaaxaabbxbbx"));
            REQUIRE(matcher.match(automaton.get(), "aabxaabxxaabxaabxx"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "xxx"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "xaxaxaxa"));
        }
        
        SECTION("group number resetting") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?|(a)|(b))\\1", params));
            REQUIRE(matcher.match(automaton.get(), "aa"));
            REQUIRE(matcher.match(automaton.get(), "bb"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "a"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "b"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "ab"));
            
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?|(a)|(b))(?|(c)|(d))\\1\\2", params));
            REQUIRE(matcher.match(automaton.get(), "acac"));
            REQUIRE(matcher.match(automaton.get(), "adad"));
            REQUIRE(matcher.match(automaton.get(), "bcbc"));
            REQUIRE(matcher.match(automaton.get(), "bdbd"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "ac"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "acaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "accc"));
            
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?|a(?|(b(c))|((d)e))|(f(?|(g)|(h))))_\\2_\\1", params));
            REQUIRE(matcher.match(automaton.get(), "abc_c_bc"));
            REQUIRE(matcher.match(automaton.get(), "ade_d_de"));
            REQUIRE(matcher.match(automaton.get(), "fg_g_fg"));
            REQUIRE(matcher.match(automaton.get(), "fh_h_fh"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abc_ab_c"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abc__bc"));
            
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?|(f(?|(g)|(h)))|a(?|(b(c))|((d)e)))_\\2_\\1", params));
            REQUIRE(matcher.match(automaton.get(), "abc_c_bc"));
            REQUIRE(matcher.match(automaton.get(), "ade_d_de"));
            REQUIRE(matcher.match(automaton.get(), "fg_g_fg"));
            REQUIRE(matcher.match(automaton.get(), "fh_h_fh"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "ade__de"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "fg_fg_"));
        }

        SECTION("named capture groups") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?<x1>a*)b(?<x2>c+)\\k<x1>b*\\k<x2>", params));
            REQUIRE(matcher.match(automaton.get(), "bcc"));
            REQUIRE(matcher.match(automaton.get(), "aabcccaabccc"));
            REQUIRE(matcher.match(automaton.get(), "aabcccaaccc"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aabcccaabcc"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aabcccaaacc"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>(
                "(?<day>[0-2][0-9]|3[01])-(?<month>0[0-9]|1[0-2])-(?<year>[0-9]{4})=\\k<month>/\\k<day>/\\k<year>", params));
            REQUIRE(matcher.match(automaton.get(), "01-04-2012=04/01/2012"));
            REQUIRE(matcher.match(automaton.get(), "22-12-1999=12/22/1999"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "01-04-2012=01/04/2012"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "01-04-12=01/04/12"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "15-04-14=04/15/14"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?<x>a+)(?<y>.*)(c+)\\k<y>\\k<x>\\2\\3", params));
            REQUIRE(matcher.match(automaton.get(), "abcbabc"));
            REQUIRE(matcher.match(automaton.get(), "aaaccaaacc"));
            REQUIRE(matcher.match(automaton.get(), "aabccabcaabcc"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aaaaccaaacc"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aabcccabcaabcc"));

            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?:\\k<A>b(?<A>a+)c)+", params));
            REQUIRE(matcher.match(automaton.get(), "bac"));
            REQUIRE(matcher.match(automaton.get(), "baacaabaaac"));
            REQUIRE(matcher.match(automaton.get(), "bacabaaaacaaaabaac"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "bacbac"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "bacaabac"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "bacabaaaacaaabac"));
            
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?|(?<x>a)|(?<x>b))\\1", params));
            REQUIRE(matcher.match(automaton.get(), "aa"));
            REQUIRE(matcher.match(automaton.get(), "bb"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "a"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "b"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "ab"));
        }
        
        SECTION("named capture groups with re-definitions") {
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?<x>a)(?:(?<&x>b)|\\k<x>)\\k<x>", params));
            REQUIRE(matcher.match(automaton.get(), "abb"));
            REQUIRE(matcher.match(automaton.get(), "aaa"));
            REQUIRE_FALSE(matcher.match(automaton.get(), ""));
            REQUIRE_FALSE(matcher.match(automaton.get(), "ab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aba"));
                        
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(\\k<x>(?<x>a+)b\\k<x>(?<&x>.))*\\k<x>", params));
            REQUIRE(matcher.match(automaton.get(), ""));
            REQUIRE(matcher.match(automaton.get(), "abacc"));
            REQUIRE(matcher.match(automaton.get(), "aabaaaaababb"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abac"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abaca"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "aabaaaaabab"));
            
            REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("((?<y>a+b)|(?<&y>a*c))c\\k<y>", params));
            REQUIRE(matcher.match(automaton.get(), "ccc"));
            REQUIRE(matcher.match(automaton.get(), "aaccaac"));
            REQUIRE(matcher.match(automaton.get(), "abcab"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "cc"));
            REQUIRE_FALSE(matcher.match(automaton.get(), "abc"));
        }
    }
    
    SECTION("basic searching") {
        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("a+b", params));
        REQUIRE(matcher.search(automaton.get(), "ab"));
        REQUIRE(matcher.search(automaton.get(), "bbaaaba"));
        REQUIRE(matcher.search(automaton.get(), "bbbbbab"));
        REQUIRE_FALSE(matcher.search(automaton.get(), "bbbaaaa"));

        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("\\w+@\\w+\\.[a-z]+", params));
        REQUIRE(matcher.search(automaton.get(), "=  hu@hu.hu  ="));
        REQUIRE(matcher.search(automaton.get(), "---     ///test@email.cz***"));
        REQUIRE(matcher.search(automaton.get(), "abc_123@x.y"));
        REQUIRE_FALSE(matcher.search(automaton.get(), "a@@b.c"));
        REQUIRE_FALSE(matcher.search(automaton.get(), "a*@a.b"));

        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("([a-zA-Z]{3}).*\\1", params));
        REQUIRE(matcher.search(automaton.get(), "abacaba"));
        REQUIRE(matcher.search(automaton.get(), "abcabaabbaac"));
        REQUIRE_FALSE(matcher.search(automaton.get(), ""));
        REQUIRE_FALSE(matcher.search(automaton.get(), "abcabaabb"));
        REQUIRE_FALSE(matcher.search(automaton.get(), "ababab"));
        
        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?:\\1b(a+)){2,}", params));
        REQUIRE(matcher.search(automaton.get(), "baaba"));
        REQUIRE(matcher.search(automaton.get(), "babaabaaaaba"));
        REQUIRE(matcher.search(automaton.get(), "baabaaaaaaba"));
        REQUIRE_FALSE(matcher.search(automaton.get(), ""));
        REQUIRE_FALSE(matcher.search(automaton.get(), "baaaba"));
        REQUIRE_FALSE(matcher.search(automaton.get(), "baba"));
    }

    SECTION("searching with anchors") {
        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("^a*b", params));
        REQUIRE(matcher.search(automaton.get(), "bbb"));
        REQUIRE(matcher.search(automaton.get(), "aaaabccc"));
        REQUIRE_FALSE(matcher.search(automaton.get(), ""));
        REQUIRE_FALSE(matcher.search(automaton.get(), "cab"));
        REQUIRE_FALSE(matcher.search(automaton.get(), "acaaab"));

        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(?:ab|cd)e+$", params));
        REQUIRE(matcher.search(automaton.get(), "xxxabeee"));
        REQUIRE(matcher.search(automaton.get(), "cde"));
        REQUIRE_FALSE(matcher.search(automaton.get(), ""));
        REQUIRE_FALSE(matcher.search(automaton.get(), "abea"));
        REQUIRE_FALSE(matcher.search(automaton.get(), "cdece"));

        REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("^ab+$", params));
        REQUIRE(matcher.search(automaton.get(), "ab"));
        REQUIRE(matcher.search(automaton.get(), "abbbb"));
        REQUIRE_FALSE(matcher.search(automaton.get(), "aba"));
        REQUIRE_FALSE(matcher.search(automaton.get(), "bab"));
    }
}

TEMPLATE_TEST_CASE("match information and group contents", "[match_info][matching][searching][avd]", MemoryAutomaton, AvdOptimizedMFA) {
    using Automaton = TestType;
    std::unique_ptr<Automaton> automaton;
    Parameters params;
    params.m_UnusedMemoriesOptEn = false;
    BacktrackMatcher<Automaton> matcher;

    MatchResult result;

    REQUIRE_FALSE(result.isReady());
    REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("a+b", params));
    REQUIRE(matcher.search(automaton.get(), "bbbaaaba", &result));
    REQUIRE(result.isReady());
    REQUIRE(result.matched());
    REQUIRE(result.getPosition() == 3);
    REQUIRE(result.getLength() == 4);
    REQUIRE(result.getMatchedString() == "aaab");

    REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("1[01]*0|0", params));
    REQUIRE(matcher.match(automaton.get(), "11010", &result));
    REQUIRE(result.isReady());
    REQUIRE(result.matched());
    REQUIRE(result.getPosition() == 0);
    REQUIRE(result.getLength() == 5);
    REQUIRE(result.getMatchedString() == "11010");

    REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("1[01]*0|0", params));
    REQUIRE_FALSE(matcher.match(automaton.get(), "11011", &result));
    REQUIRE_FALSE(result.matched());

    REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(.+)\\1", params));
    REQUIRE(matcher.search(automaton.get(), "abcdababc", &result));
    REQUIRE(result.getPosition() == 4);
    REQUIRE(result.getMatchedString() == "abab");
    if (!std::is_same<TestType, AvdOptimizedMFA>::value) {
        // AvdOptimizedMFA does not provide information about capture group contents
        REQUIRE(result[1] == "ab");
    }

    REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("<(\\w+) href=\"([.a-zA-Z]+)\">(.*)</\\1>", params));
    REQUIRE(matcher.search(automaton.get(), "<div><a href=\"example.url\">text</a></div>", &result));
    REQUIRE(result.getPosition() == 5);
    REQUIRE(result.getMatchedString() == "<a href=\"example.url\">text</a>");
    REQUIRE(result[0] == result.getMatchedString());
    if (!std::is_same<TestType, AvdOptimizedMFA>::value) {
        REQUIRE(result[1] == "a");
        REQUIRE(result[2] == "example.url");
        REQUIRE(result[3] == "text");
    }
}

TEST_CASE("matching lazy quantifiers", "[matching][searching][match_semantics]") {
    using Automaton = MemoryAutomaton;
    std::unique_ptr<Automaton> automaton;
    Parameters params;
    params.m_UnusedMemoriesOptEn = false;
    BacktrackMatcher<Automaton> matcher;
    MatchResult result;

    REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("ab??", params));
    REQUIRE(matcher.search(automaton.get(), "ab", &result));
    REQUIRE(result.getPosition() == 0);
    REQUIRE(result.getMatchedString() == "a");

    REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("ab+?", params));
    REQUIRE(matcher.search(automaton.get(), "aaabbbbb", &result));
    REQUIRE(result.getPosition() == 2);
    REQUIRE(result.getMatchedString() == "ab");

    REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("ab*?", params));
    REQUIRE(matcher.search(automaton.get(), "abbb", &result));
    REQUIRE(result.getPosition() == 0);
    REQUIRE(result.getMatchedString() == "a");

    REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("a{2,4}?", params));
    REQUIRE(matcher.search(automaton.get(), "aaaa", &result));
    REQUIRE(result.getMatchedString() == "aa");

    REQUIRE_NOTHROW(automaton = std::make_unique<Automaton>("(a*)(b+?)([^c]*)(c*?)", params));
    REQUIRE(matcher.match(automaton.get(), "bbbccc", &result));
    REQUIRE(result[1] == "");
    REQUIRE(result[2] == "b");
    REQUIRE(result[3] == "bb");
    REQUIRE(result[4] == "ccc");

    REQUIRE(matcher.match(automaton.get(), "aaabbxccc", &result));
    REQUIRE(result[1] == "aaa");
    REQUIRE(result[2] == "b");
    REQUIRE(result[3] == "bx");
    REQUIRE(result[4] == "ccc");

    REQUIRE(matcher.search(automaton.get(), "01abbxccc", &result));
    REQUIRE(result.getPosition() == 2);
    REQUIRE(result[1] == "a");
    REQUIRE(result[2] == "b");
    REQUIRE(result[3] == "bx");
    REQUIRE(result[4] == "");
}

TEMPLATE_TEST_CASE("unused memories optimization", "[matching][optimization]", BacktrackMatcher<MemoryAutomaton>, BFSMatcher<MemoryAutomaton>) {
    MFA_Builder mfaBuilder;
    Parameters params;
    std::unique_ptr<MemoryAutomaton> automaton;
    TestType matcher;
    
    params.m_UnusedMemoriesOptEn = true;
    
    REQUIRE_NOTHROW(automaton = mfaBuilder.build("a(b|c)d", params));
    REQUIRE(automaton->getNumMemories() == 0);
    REQUIRE(matcher.match(automaton.get(), "abd"));
    REQUIRE(matcher.match(automaton.get(), "acd"));
    REQUIRE_FALSE(matcher.match(automaton.get(), "abc"));
    
    REQUIRE_NOTHROW(automaton = mfaBuilder.build("(.*)(.+)\\2", params));
    REQUIRE(automaton->getNumMemories() == 1);
    REQUIRE(matcher.match(automaton.get(), "aaa"));
    REQUIRE(matcher.match(automaton.get(), "abababab"));
    REQUIRE_FALSE(matcher.match(automaton.get(), ""));
    REQUIRE_FALSE(matcher.match(automaton.get(), "abc"));
    
    REQUIRE_NOTHROW(automaton = mfaBuilder.build("(a*(b+))+\\2", params));
    REQUIRE(automaton->getNumMemories() == 1);
    REQUIRE(matcher.match(automaton.get(), "bb"));
    REQUIRE(matcher.match(automaton.get(), "abbaabb"));
    REQUIRE(matcher.match(automaton.get(), "abbaabbb"));
    REQUIRE_FALSE(matcher.match(automaton.get(), "aaaa"));
    
    REQUIRE_NOTHROW(automaton = mfaBuilder.build("(\\2a(b+))+x(x)(y)\\4", params));
    REQUIRE(automaton->getNumMemories() == 2);
    REQUIRE(matcher.match(automaton.get(), "abbabxxyy"));
    REQUIRE(matcher.match(automaton.get(), "abbbbbbabxxyy"));
    REQUIRE_FALSE(matcher.match(automaton.get(), "ababxxyy"));
    
    REQUIRE_NOTHROW(automaton = mfaBuilder.build("(a(b)c)d\\1", params));
    REQUIRE(automaton->getNumMemories() == 1);
    REQUIRE_NOTHROW(automaton->finalize());
    REQUIRE(automaton->getNumMemories() == 1);
    REQUIRE(matcher.match(automaton.get(), "abcdabc"));
    REQUIRE_FALSE(matcher.match(automaton.get(), "abcdb"));
}

TEST_CASE("active variable degree computation", "[avd]") {
    std::unique_ptr<AvdOptimizedMFA> automaton;
    BacktrackMatcher<AvdOptimizedMFA> matcher;

    REQUIRE_NOTHROW(automaton = std::make_unique<AvdOptimizedMFA>("a(b|c)d"));
    REQUIRE(automaton->getActiveVariableDegree() == 0);

    REQUIRE_NOTHROW(automaton = std::make_unique<AvdOptimizedMFA>("(.*)\\1(.*)\\2(.*)\\3"));
    REQUIRE(automaton->getActiveVariableDegree() == 1);

    REQUIRE_NOTHROW(automaton = std::make_unique<AvdOptimizedMFA>("(.*)(.*)\\2\\1"));
    REQUIRE(automaton->getActiveVariableDegree() == 2);

    REQUIRE_NOTHROW(automaton = std::make_unique<AvdOptimizedMFA>("(.*)(.*)\\1\\2"));
    REQUIRE(automaton->getActiveVariableDegree() == 2);

    REQUIRE_NOTHROW(automaton = std::make_unique<AvdOptimizedMFA>("(?:(a+)|(b))\\2(?:(a*)\\3)+\\1"));
    REQUIRE(automaton->getActiveVariableDegree() == 2);

    REQUIRE_NOTHROW(automaton = std::make_unique<AvdOptimizedMFA>("(a*)(b*)(?:\\2(c*))*(d*)\\1\\3\\4"));
    REQUIRE(automaton->getActiveVariableDegree() == 3);

    REQUIRE_NOTHROW(automaton = std::make_unique<AvdOptimizedMFA>("(?:(.+){5,}|(.*)+)(?:\\1*(.+)|(.*))?\\2\\3+\\4?"));
    REQUIRE(automaton->getActiveVariableDegree() == 3);

    REQUIRE_NOTHROW(automaton = std::make_unique<AvdOptimizedMFA>("(?:(?:(?<x>a+)(b+))|(c+)|(?:(?<&x>b+)|(c+)))(a+)\\1\\2\\3\\4\\5"));
    REQUIRE(automaton->getActiveVariableDegree() == 5);
}
