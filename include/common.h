/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef COMMON_H
#define COMMON_H

#include "config.h"
#include <string>
#include <locale>

#ifdef UNICODE_SUPPORT

#include <cstdint>
#include <utfcpp/utf8.h>

namespace mfa {

typedef uint32_t char_t;
typedef utf8::iterator<std::string::iterator> iterator_t;

} /* namespace mfa */

#else

namespace mfa {

typedef char char_t;
typedef const char * iterator_t;

} /* namespace mfa */

#endif

namespace mfa {

/**
 * Set of utilities to manipulate with (unicode) iterators, strings and characters. 
 * 
 * These functions are used to abstract away whether or not unicode (UTF-8) support is enabled. 
 * If the library is compiled with unicode support, these functions make use of the utfcpp library. 
 */
class StringUtils {
public:
    /**
     * Creates iterator over given string.
     */
    static iterator_t makeIterator(std::string & str);
    
    /**
     * Checks whether iterator points at the beginning of given string.
     */
    static bool isAtStart(const iterator_t & iter, const std::string & str);
    
    /**
     * Checks whether iterator points at the end of given string.
     */
    static bool isAtEnd(const iterator_t & iter, const std::string & str);

    /**
     * Gets number of characters between begin and end iterators.
     * 
     * @note For unicode this may not be the same as site of the string in memory. 
     */
    static int getDistance(const iterator_t & begin, const iterator_t & end);
    
    /**
     * Constructs string from begin and end iterators. 
     */
    static std::string getString(const iterator_t & begin, const iterator_t & end);
    
    /**
     * Converts character to std::string.
     * 
     * If chr is unicode character, the resulting string size may be more than one.
     */
    static std::string charToString(char_t chr);
    
    /**
     * Compute hash for given iterator.
     */
    static size_t hashIter(const iterator_t & iter, const std::string & str);
    
    /**
     * Append character at the end of given string (in-place).
     */
    static void append(char_t character, std::string & string);

    /**
     * Checks if given character is in the character class. 
     */
    static bool testCharacterClass(char_t character, std::ctype_base::mask classMask);
};

} /* namespace mfa */

#endif /* COMMON_H */
