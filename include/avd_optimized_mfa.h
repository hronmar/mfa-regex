/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef AVD_OPTIMIZED_MFA_H
#define AVD_OPTIMIZED_MFA_H

#include "memory_automaton.h"
#include "avd_computation.h"

#include <algorithm>
#include <cassert>
#include <queue>

namespace mfa {

static const int MEM_UNUSED = -1;

/**
 * Implementation of bounded active variable degree technique. 
 */
class AvdOptimizedMFA {
public:

    AvdOptimizedMFA(std::unique_ptr<MemoryAutomaton> automaton);

    AvdOptimizedMFA(const std::string & pattern, Parameters params = Parameters());

    using Transition = typename MemoryAutomaton::Transition;
    using Memory = typename MemoryAutomaton::Memory;
    using State = typename MemoryAutomaton::State;

    struct Configuration {
        MemoryAutomaton::Configuration m_OrigConf;
        std::vector<int> m_MemoryList;
        const int m_NumOrigMemories;

        Configuration(MemoryAutomaton::Configuration origConf, int numNeededMemories, int numOrigMemories)
            : m_OrigConf(std::move(origConf)), m_MemoryList(numNeededMemories, MEM_UNUSED), m_NumOrigMemories(numOrigMemories) {
        }

        State * getState() const {
            return m_OrigConf.m_State;
        }
        
        iterator_t & inputIter() {
            return m_OrigConf.m_InputIter;
        }

        std::string getMemoryContents(int i) const {
            const int idx = getOrigMemory(i);
            if (idx == MEM_UNUSED)
                return "";
            return m_OrigConf.getMemoryContents(idx);
        }

        int getNumberOfMemories() const {
            return m_NumOrigMemories;
        }

        bool operator==(const Configuration & other) const {
            if (m_OrigConf.m_State != other.m_OrigConf.m_State
                || m_OrigConf.m_InputIter != other.m_OrigConf.m_InputIter
                || m_OrigConf.m_Counters != other.m_OrigConf.m_Counters)
                return false;
            for (int mem = 0; mem < m_NumOrigMemories; mem++) {
                if (getMemoryContents(mem) != other.getMemoryContents(mem))
                    return false;
            }
            return true;
        }

        bool operator!=(const Configuration & other) const {
            bool result = !(*this == other); // reuse equals operator
            return result;
        }

        struct hash {
            size_t operator()(const Configuration & conf) const {
                size_t hash = conf.m_OrigConf.getHashBase();
                for (int i = 0; i < conf.m_NumOrigMemories; i++) {
                    const int mem = conf.getOrigMemory(i);
                    if (mem != MEM_UNUSED) {
                        Utils::hashCombineInto(hash, StringUtils::hashIter(conf.m_OrigConf.m_Memories[mem].m_Begin, conf.m_OrigConf.m_Input));
                        Utils::hashCombineInto(hash, StringUtils::hashIter(conf.m_OrigConf.m_Memories[mem].m_End, conf.m_OrigConf.m_Input));
                        Utils::hashCombineInto(hash, std::hash<bool>{}(conf.m_OrigConf.m_Memories[mem].m_Open));
                    }
                }
                return hash;
            }
        };

        /**
         * Allocate new memory for an old memory with given index.
         */
        int allocateNewMemory(int oldMemIdx) {
            int newMemIdx;
            for (newMemIdx = 0; newMemIdx < static_cast<int>(m_MemoryList.size()); newMemIdx++) {
                if (m_MemoryList[newMemIdx] == MEM_UNUSED) {
                    m_MemoryList[newMemIdx] = oldMemIdx;
                    return newMemIdx;
                }
            }
            assert(false); // should never reach here
            return newMemIdx;
        }

        /**
         * Returns which of the avd(alpha) new memories currently handles the given old memory, 
         * or MEM_UNUSED if there is no such memory.
         */
        int getOrigMemory(int memIdx) const {
            for (size_t i = 0; i < m_MemoryList.size(); i++)
                if (m_MemoryList[i] == memIdx)
                    return i;

            return MEM_UNUSED;
        }

        /**
         * Frees all memories in memory list that are no longer active.
         */
        void clearUnused(const std::vector<bool> & activeMemories) {
            for (size_t i = 0; i < m_MemoryList.size(); i++)
                if (m_MemoryList[i] != MEM_UNUSED && !activeMemories[m_MemoryList[i]])
                    m_MemoryList[i] = MEM_UNUSED;
        }
    };

    Parameters getParameters() const;

    void prepareStates(int numStates);
    
    State * addState();

    void addTransition(Transition tr, State * from, State * to, bool lazy = false);

    void addMemory();

    void setInitialState(State * state);

    void setFinalState(State * state);

    void finalize();

    int getNumMemories() const;

    int getActiveVariableDegree() const;

    void setSyntaxTree(SyntaxTreeNode * syntaxTree);

    SyntaxTreeNode * getSyntaxTree() const;

    void setInput(const std::string & str);

    void setInput(std::string && str);
    
    std::string & getInput();

    Configuration getInitialConfiguration();

    bool isAcceptingConfiguration(const Configuration & conf, bool searching = false) const;

    template <typename F>
    void forEachNextConfiguration(Configuration conf, F func) const {
        State * from = conf.m_OrigConf.m_State;
        from->forEachTransition([&] (Transition tr, State * to) {
            Configuration newConf(conf);

            if (tr.m_Type == Transition::MEMORY_OPEN) {
                if (m_ActiveVariableSets.at(to)[tr.m_MemIndex]) {
                    // allocate new memory
                    tr.m_MemIndex = newConf.allocateNewMemory(tr.m_MemIndex);
                } else {
                    // do not open unused memory
                    tr.m_Type = Transition::EPSILON;
                }
            } else if (tr.m_Type == Transition::MEMORY_CLOSE || tr.m_Type == Transition::MEMORY_RECALL) {
                int memIdx;
                if (m_ActiveVariableSets.at(from)[tr.m_MemIndex]
                    && (memIdx = newConf.getOrigMemory(tr.m_MemIndex)) != MEM_UNUSED) {
                    tr.m_MemIndex = memIdx;
                } else {
                    // if this memory is unused now (and thus could not have string bound to it before), 
                    // treat this also as epsilon transition (epsilon semantics) for recall,
                    // MEM_UNUSED will not occur for close transitions (closed memory was always in use)
                    tr.m_Type = Transition::EPSILON;
                }
            }
            if (executeTransition(newConf, tr, to)) {
                // clear memories that will not be recalled from the new state
                newConf.clearUnused(m_ActiveVariableSets.at(to));
                    func(std::move(newConf));
            }
        });
    }

    std::vector<Configuration> getNextConfigurations(const Configuration & conf) const;

    bool executeTransition(Configuration & conf, const Transition & tr, State * to) const;

private:

    std::unique_ptr<MemoryAutomaton> m_Wrappee;

    std::unordered_map<State *, std::vector<bool>> m_ActiveVariableSets;
    int m_ActiveVariableDegree;
    int m_OrigNumVaribles;

};

} /* namespace mfa */

#endif /* AVD_OPTIMIZED_MFA_H */
