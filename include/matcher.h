/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MATCHER_H
#define MATCHER_H

#include "memory_automaton.h"
#include "match_result.h"

namespace mfa {

/**
 * Base class for matching algorithms.
 * 
 * This is an abstract class defining the interface for classes implementing regex
 * matching algorithms. Use one of the derived classes to match. 
 */
template <typename Automaton>
class Matcher {
public:

    virtual ~Matcher() = default;

    /**
     * Attempts to match an entire input on given memory automaton. 
     * 
     * Optionally also provides details about the match, if pointer to MatchResult object is given. 
     * @return True iff the entire input is accepted by given MFA
     */
    virtual bool match(Automaton * automaton, std::string input, MatchResult * result = nullptr) = 0;

    /**
     * Attempts to match any part of input (i.e. performs search) on given memory automaton. 
     * 
     * Optionally also provides details about the match, if pointer to MatchResult object is given. 
     * @return True iff any part of input is accepted by given MFA
     */
    virtual bool search(Automaton * automaton, std::string input, MatchResult * result = nullptr) = 0;
    
    typedef Automaton automaton_t;
};

} /* namespace mfa */

#endif /* MATCHER_H */
