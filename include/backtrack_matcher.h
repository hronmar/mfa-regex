/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef BACKTRACK_MATCHER_H
#define BACKTRACK_MATCHER_H

#include "matcher.h"
#include "common.h"

#include <unordered_set>

namespace mfa {

/**
 * Recursive backtracking based matcher.
 * 
 * This class implements backtracking algorithm for regex matching. It recursively tries all 
 * transitions starting from the initial configuration. Some visited configurations are cached to prevent 
 * infinite loops (e.g. in Kleene star constructs). Optionally, all visited configurations may be cached 
 * (see setCacheAllVisited()), in which case the algorithm behaves as depth-first search, and if the 
 * number of automaton memories is bounded by a constant, its asymptotic time complexity is polynomial. 
 */
template <typename Automaton>
class BacktrackMatcher : public Matcher<Automaton> {
public:

    /**
     * @param cacheAllVisited Sets whether all visited configurations should be cached, see also setCacheAllVisited().
     */
    BacktrackMatcher(bool cacheAllVisited = false) : m_CacheAllVisited(cacheAllVisited) {
    }

    virtual ~BacktrackMatcher() = default;

    using Configuration = typename Automaton::Configuration;
    
    virtual bool match(Automaton * automaton, std::string input, MatchResult * result = nullptr) override {
        m_Automaton = automaton;
        m_Automaton->setInput(std::move(input));
        m_VisitedCache.clear();
        m_Result = result;
        m_MatchStart = StringUtils::makeIterator(m_Automaton->getInput());
        bool match = matchFunc(m_Automaton->getInitialConfiguration());
        if (!match && result != nullptr)
            result->fillFailedMatchResult();
        return match;
    }

    virtual bool search(Automaton * automaton, std::string input, MatchResult * result = nullptr) override {
        m_Automaton = automaton;
        m_Automaton->setInput(std::move(input));
        m_VisitedCache.clear();
        m_Result = result;

        Configuration initConf(m_Automaton->getInitialConfiguration());

        do {
            m_MatchStart = initConf.inputIter();
            if (matchFunc(initConf, true))
                return true;
            if (!StringUtils::isAtEnd(initConf.inputIter(), m_Automaton->getInput()))
                ++initConf.inputIter();
        } while (!StringUtils::isAtEnd(initConf.inputIter(), m_Automaton->getInput()));

        if (result != nullptr)
            result->fillFailedMatchResult();
        return false;
    }

    /**
     * Sets whether all visited configurations should be cached.
     * 
     * By default, the algorithm caches only some of the visited configurations
     * to prevent getting stuck (e.g. inside nested Kleene star constructs), but if 
     * this option is set to true, it will cache all the visited configurations, 
     * thus behaving like DFS search algorithm. This may speed up matching for some 
     * inputs, but it also increases memory usage. 
     */
    void setCacheAllVisited(bool cacheAllVisited) {
        m_CacheAllVisited = cacheAllVisited;
    }

private:

    Automaton * m_Automaton;
    std::unordered_set<Configuration, typename Configuration::hash> m_VisitedCache;
    bool m_CacheAllVisited;

    // for filling MatchResult from matchFunc
    MatchResult * m_Result = nullptr;
    iterator_t m_MatchStart;

    bool matchFunc(Configuration conf, bool searching = false) {
        if (m_Automaton->isAcceptingConfiguration(conf, searching)) {
            // we will eventually leave all recursive calls and return true, 
            // but if result info should be provided, we do it here
            if (m_Result != nullptr)
                m_Result->fillMatchResult(&conf, m_Automaton->getInput(), m_MatchStart);
            return true;
        }
        if (m_CacheAllVisited
            || (conf.getState()->m_NextState2 != nullptr 
                && conf.getState()->m_Transition.m_Type == MemoryAutomaton::Transition::EPSILON))
            m_VisitedCache.insert(conf);
        bool match = false;
        m_Automaton->forEachNextConfiguration(std::move(conf), [&] (Configuration cnf) {
            if (!match && m_VisitedCache.count(cnf) == 0 && matchFunc(std::move(cnf), searching)) {
                match = true;
            }
        });
        return match;
    }
};

} /* namespace mfa */

#endif /* BACKTRACK_MATCHER_H */
