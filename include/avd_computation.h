/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef AVD_COMPUTATION_H
#define AVD_COMPUTATION_H

#include "memory_automaton.h"

namespace mfa {

/**
 * For given memory automaton, computes active variable degree and for each state the sets of active variables. 
 * 
 * @param[in] mfa The memory automaton
 * @param[out] activeVariableSets Output variable for the active variable sets
 * @return The computed active variable degree
 */
int computeActiveVariables(mfa::MemoryAutomaton * mfa,
    std::unordered_map<mfa::MemoryAutomaton::State *, std::vector<bool>> & activeVariableSets);

} /* namespace mfa */

#endif /* AVD_COMPUTATION_H */
