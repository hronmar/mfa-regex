/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MEMORY_AUTOMATON_H
#define MEMORY_AUTOMATON_H

#include "common.h"
#include "utils.h"
#include "character_set.h"
#include "counting_constraint.h"
#include "exceptions.h"

#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace mfa {

struct SyntaxTreeNode; // forward declaration

/**
* Parameters for memory automaton.
*/
struct Parameters {

    /**
     * Whether unused memories will be optimized away.
     * 
     * @warning If this option is used, contents of the memories that are optimized 
     * away will be reported as empty string in MatchResult.
     */
    bool m_UnusedMemoriesOptEn = false;

    /**
     * Enables pattern optimizations.
     * 
     * If enabled, during construction, the pattern will be transformed into an
     * equivalent pattern with better matching performance if possible. Enabling 
     * this will slightly increase pattern compilation time, but might result in 
     * decreased matching time in some cases. Enabled by default. 
     */
    bool m_EnablePatternOptimizations = true;
};

/**
 * Class implementing memory automata for regex matching.
 * 
 * @note This class is intended for memory automata constructed for matching regex patterns
 *        and makes strong assumptions about possible automaton structure.
 */
class MemoryAutomaton {
public:

    /**
     * Represents outgoing transition in memory automaton.
     * 
     * The reachable states are stored in State along with the transition.
     */
    struct Transition {
        enum TransitionType {
            SYMBOL, EPSILON, DOT, CHARACTER_SET, MEMORY_OPEN, MEMORY_CLOSE, MEMORY_RECALL, COUNTER_INC, COUNTER_OUT,
            START_ANCHOR, END_ANCHOR, WORD_BOUNDARY, NOT_WORD_BOUNDARY
        };

        TransitionType m_Type;
        union {
            char_t m_Symbol;
            int m_MemIndex;
            CharacterSet * m_CharacterSet;
            CountingConstraint m_CountingCn;
        };

        Transition(TransitionType type) : m_Type(type) { }
        Transition(TransitionType type, char_t symbol) : m_Type(type), m_Symbol(symbol) { }
        Transition(TransitionType type, int memIndex) : m_Type(type), m_MemIndex(memIndex) { }
        Transition(TransitionType type, CharacterSet * characterSet) : m_Type(type), m_CharacterSet(characterSet) { }
        Transition(TransitionType type, CountingConstraint cnt) : m_Type(type), m_CountingCn(cnt) { }

        bool isMemoryTransition() const {
            return (m_Type == TransitionType::MEMORY_OPEN || m_Type == TransitionType::MEMORY_CLOSE || m_Type == TransitionType::MEMORY_RECALL);
        }
    };

    /**
     * Representation of memory automaton state.
     */
    struct State {
        Transition m_Transition;
        State * m_NextState1;
        State * m_NextState2;
        
        State() : m_Transition(Transition::EPSILON), m_NextState1(nullptr), m_NextState2(nullptr) {
        }
        
        ~State() {
            if (m_Transition.m_Type == Transition::CHARACTER_SET)
                delete m_Transition.m_CharacterSet;
        }
        
        void addTransition(const Transition & transition, State * nextState) {
            if (m_NextState1 == nullptr) {
                m_NextState1 = nextState;
            } else if (m_NextState2 == nullptr) {
                m_NextState2 = nextState;
            } else
                throw automaton_error("max outdegree should always be 2");
            m_Transition = transition;
        }

        void addLazyTransition(const Transition & transition, State * nextState) {
            if (m_NextState2 == nullptr) {
                m_NextState2 = nextState;
            } else if (m_NextState1 == nullptr) {
                m_NextState1 = nextState;
            } else
                throw automaton_error("max outdegree should always be 2");
            m_Transition = transition;
        }
        
        template <typename F>
        void forEachTransition(F func) const {
            if (m_NextState1 != nullptr) {
                func(m_Transition, m_NextState1);
                if (m_NextState2 != nullptr)
                    func(m_Transition, m_NextState2);
            }
        }
    };

    /**
     * Used to store memory content and memory status.
     */
    struct Memory {
        iterator_t m_Begin;
        iterator_t m_End;
        bool m_Open = false;
        
        bool operator==(const Memory & other) const {
            return (m_Open == other.m_Open 
                && m_Begin == other.m_Begin && m_End == other.m_End);
        }

        bool operator!=(const Memory & other) const {
            bool result = !(*this == other); // reuse equals operator
            return result;
        }
    };

    /**
     * Represents memory automaton configuration.
     */
    struct Configuration {
        State * m_State;
        iterator_t m_InputIter;
        const std::string & m_Input;
        std::vector<Memory> m_Memories;
        std::vector<int> m_Counters;

        Configuration(State * state, iterator_t inputIter, const std::string & input, int numMemories, int numCounters)
            : m_State(state), m_InputIter(inputIter), m_Input(input), m_Memories(numMemories), m_Counters(numCounters, 0) {
            for (Memory & mem : m_Memories)
                mem.m_Begin = mem.m_End = m_InputIter;
        }

        State * getState() const {
            return m_State;
        }

        iterator_t & inputIter() {
            return m_InputIter;
        }

        std::string getMemoryContents(int i) const {
            return StringUtils::getString(m_Memories[i].m_Begin, m_Memories[i].m_End);
        }

        int getNumberOfMemories() const {
            return m_Memories.size();
        }

        bool operator==(const Configuration & other) const {
            return (m_State == other.m_State && m_InputIter == other.m_InputIter
                && m_Memories == other.m_Memories && m_Counters == other.m_Counters);
        }
        
        bool operator!=(const Configuration & other) const {
            bool result = !(*this == other); // reuse equals operator
            return result;
        }
        
        size_t getHashBase() const {
            size_t hash = Utils::hashCombine(std::hash<State *>()(m_State), 
                    StringUtils::hashIter(m_InputIter, m_Input));
            for (int cnt : m_Counters) {
                Utils::hashCombineInto(hash, std::hash<int>{}(cnt));
            }
            return hash;
        }

        size_t getHashMemories(size_t seed) const {
            size_t hash = seed;
            for (int mem = 0; mem < static_cast<int>(m_Memories.size()); mem++) {
                Utils::hashCombineInto(hash, StringUtils::hashIter(m_Memories[mem].m_Begin, m_Input));
                Utils::hashCombineInto(hash, StringUtils::hashIter(m_Memories[mem].m_End, m_Input));
                Utils::hashCombineInto(hash, std::hash<bool>{}(m_Memories[mem].m_Open));
            }
            return hash;
        }

        struct hash {
            size_t operator()(const Configuration & conf) const {
                return conf.getHashMemories(conf.getHashBase());
            }
        };
    };

    MemoryAutomaton(Parameters params = Parameters());
    MemoryAutomaton(const std::string & pattern, Parameters params = Parameters());

    MemoryAutomaton(const MemoryAutomaton & other) = delete;
    MemoryAutomaton(MemoryAutomaton && other) = delete;
    MemoryAutomaton & operator=(const MemoryAutomaton & other) = delete;
    MemoryAutomaton & operator=(MemoryAutomaton && other) = delete;

    virtual ~MemoryAutomaton();

    Parameters getParameters() const;

    void prepareStates(int numStates);
    State * addState();
    void addTransition(Transition tr, State * from, State * to, bool lazy = false);
    void addMemory();
    
    void setInitialState(State * state);
    void setFinalState(State * state);

    void finalize();
    
    int getNumMemories() const;
    
    void setSyntaxTree(SyntaxTreeNode * syntaxTree);
    SyntaxTreeNode * getSyntaxTree() const;
    void setInput(const std::string & str);
    void setInput(std::string && str);
    std::string & getInput();

    Configuration getInitialConfiguration();
    bool isAcceptingConfiguration(const Configuration & conf, bool searching = false) const;

    template <typename F>
    void forEachNextConfiguration(Configuration conf, const F & func) const {
        State * st2 = conf.m_State->m_NextState2;
        if (conf.m_State->m_NextState1 != nullptr && executeTransition(conf, conf.m_State->m_Transition, conf.m_State->m_NextState1)) {
            if (st2 == nullptr) {
                func(std::move(conf));
            } else { //st2 != nullptr
                func(conf); // must copy here
                conf.m_State = st2; // here we use the fact that all transitions originating from same state are same
                func(std::move(conf));
            }
        }
    }

    std::vector<Configuration> getNextConfigurations(const Configuration & conf) const;

    bool executeTransition(Configuration & conf, const Transition & tr, State * to) const;

    friend int computeActiveVariables(MemoryAutomaton *, std::unordered_map<State *, std::vector<bool>> &);
    friend class AvdOptimizedMFA;

private:
    
    Parameters m_Parameters;

    int m_NumMemories;
    int m_NumCounters;
    
    std::vector<State> m_States;

    State * m_InitState;
    State * m_FinalState;
    SyntaxTreeNode * m_SyntaxTree;

    std::string m_Input;

    std::unordered_set<int> m_RecalledMemories;
    int m_NumUsedMemories;

    bool isAtWordBoundary(const Configuration & conf) const;

    void removeUnusedMemories();
};

} /* namespace mfa */

#endif /* MEMORY_AUTOMATON_H */
