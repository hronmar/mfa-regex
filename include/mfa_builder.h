/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MFA_BUILDER_H
#define MFA_BUILDER_H

#include "memory_automaton.h"
#include "common.h"
#include "counting_constraint.h"
#include "syntax_tree.h"

#include <set>
#include <sstream>
#include <stack>
#include <map>
#include <vector>
#include <memory>

namespace mfa {

// forward declaration of parser
class parser;

/**
 * Fragment of constructed memory automaton.
 */
struct MFA_Fragment {
    // syntax tree node, also stores the initial and the final state of this fragment
    SyntaxTreeNode * m_Node;

    int m_CounterIdx;

    std::set<int> m_Var;

    MFA_Fragment(MemoryAutomaton * mfa, SyntaxTreeNode * node, 
        MFA_Fragment left = MFA_Fragment(), MFA_Fragment right = MFA_Fragment())
        : m_Node(node), m_CounterIdx(std::max(left.m_CounterIdx, right.m_CounterIdx)), m_Var(left.m_Var) {
        m_Node->m_Tin = mfa->addState();
        m_Node->m_Tout = mfa->addState();
        m_Var.insert(right.m_Var.begin(), right.m_Var.end()); // m_Var = left.m_Var union right.m_Var
    }

    MFA_Fragment() : m_Node(nullptr), m_CounterIdx(0) {
    }

    /**
     * Initial state of this automaton fragment.
     */
    MemoryAutomaton::State * Tin() const {
        return m_Node->m_Tin;
    }

    /**
     * Final state of this automaton fragment.
     */
    MemoryAutomaton::State * Tout() const {
        return m_Node->m_Tout;
    }
};

/**
 * Class for memory automaton construction (from pattern string).
 */
class MFA_Builder {
    friend class parser;

public:

    MFA_Builder();

    /**
     * Build the input pattern into given instance of memory automaton.
     */
    void buildInto(const std::string & pattern, MemoryAutomaton * automaton);

    /**
     * Build the input pattern and return new instance of memory automaton.
     */
    std::unique_ptr<MemoryAutomaton> build(const std::string & pattern, Parameters params = Parameters());

private:

    bool parse();
    
    int nextCGIndex();
    void resetCGIndex();
    void pushCGIndex();
    void popCGIndex();
    void restoreCGIndex();
    int peekNextCGIndex() const;

    void resetCtx();

    MFA_Fragment compile(AtomNode * node);
    MFA_Fragment compile(PosIterNode * node);
    MFA_Fragment compile(ConcatNode * node);
    MFA_Fragment compile(AlterNode * node);
    MFA_Fragment compile(CaptureNode * node);
    MFA_Fragment compile(BackrefNode * node);
    MFA_Fragment compile(EpsilonNode * node);
    MFA_Fragment compile(OptionalNode * node);
    MFA_Fragment compile(IterNode * node);
    MFA_Fragment compile(DotNode * node);
    MFA_Fragment compile(CharacterSetNode * node);
    MFA_Fragment compile(CounterNode * node);
    MFA_Fragment compile(StartAnchorNode * node);
    MFA_Fragment compile(EndAnchorNode * node);
    MFA_Fragment compile(WordBoundaryNode * node);
    MFA_Fragment compile(SyntaxTreeNode * node);
    SyntaxTreeNode * optimize(SyntaxTreeNode * node);
    void buildResult(SyntaxTreeNode * node);

    int defineNamedCaptureGroup(const std::string & name, int k);
    int getNamedCaptureGroup(const std::string & name, bool defineNew = false);
    SyntaxTreeNode * makeNamedBackref(const std::string & name);
    void resolveNamedCaptures();

    static const int FIRST_CG_INDEX = 1;
    std::string m_Pattern;
    int m_NextCGIndex;
    std::stack<int> m_StoredCGIndeces; // saved cg. indeces (for branch reset (?|...) groups)
    std::map<std::string, int> m_NamedCaptures; // map of named capture groups (name -> cg. index)
    std::set<int> m_NamedCapturesIndeces; // cg. indeces with name assigned to them (needed for check in (?|...))
    std::vector<std::pair<std::string, BackrefNode *>> m_UnresolvedCaptures;

    MemoryAutomaton * m_Automaton;
};

} /* namespace mfa */

#endif /* MFA_BUILDER_H */
