/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef LEXER_H
#define LEXER_H

#include "common.h"
#include "parser.h"

#include <climits>
#include <sstream>

namespace mfa {

parser::symbol_type yylex(Lexer * lexer);

/**
 * Lexical analyzer for regex expressions.
 */
class Lexer {
public:
    Lexer(const std::string & input);

    /**
     * Reads the next token.
     */
    parser::symbol_type getNext();

private:
    std::string m_Input;
    iterator_t m_InputIter;
    char_t m_Character;

    static const CharacterSet SPECIAL_CHARACTERS;
    static const char_t EOI = static_cast<char_t>(-1);

    void readCharacter();
    void readCharacter(char_t requestedCharacter);

    int readNumber(int base = 10, int maxLen = INT_MAX);

    void readIdentifier(std::string & ident);

    parser::symbol_type handleEscape();
};

} /* namespace mfa */

#endif /* LEXER_H */
