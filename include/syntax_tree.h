/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SYNTAX_TREE_H
#define SYNTAX_TREE_H

#include "common.h"
#include "memory_automaton.h"

#include <cassert>
#include <ostream>

namespace mfa {

/**
 * Syntax tree node.
 */
struct SyntaxTreeNode {
    enum NodeType {
        ATOM, EPSILON, DOT, CHAR_SET, OPTIONAL, POS_ITER, ITER, CONCAT, ALTER, CAPTURE, BACKREF, COUNTER, 
        START_ANCHOR, END_ANCHOR, WORD_BOUNDARY
    };

    SyntaxTreeNode(NodeType type, SyntaxTreeNode * left = nullptr, SyntaxTreeNode * right = nullptr)
        : m_NodeType(type), m_Left(left), m_Right(right), m_Tin(nullptr), m_Tout(nullptr) {
    }

    virtual ~SyntaxTreeNode() {
        delete m_Left;
        delete m_Right;
    }

    NodeType m_NodeType;
    SyntaxTreeNode * m_Left;
    SyntaxTreeNode * m_Right;
    MemoryAutomaton::State * m_Tin;
    MemoryAutomaton::State * m_Tout;

    void print(std::ostream & out, int indent = 0) const {
        for (int i = 0; i < indent; i++)
            out << "  ";
        doPrint(out);
        if (m_Left != nullptr)
            m_Left->print(out, indent + 1);
        if (m_Right != nullptr)
            m_Right->print(out, indent + 1);
    }

    virtual int getNumStates() const {
        // most constructs require 2 states (+ states of child nodes)
        int numStates = 2;
        if (m_Left != nullptr)
            numStates += m_Left->getNumStates();
        if (m_Right != nullptr)
            numStates += m_Right->getNumStates();
        return numStates;
    }

protected:
    virtual void doPrint(std::ostream & out) const = 0;
};

struct AtomNode : public SyntaxTreeNode {
    AtomNode(char_t symbol) : SyntaxTreeNode(SyntaxTreeNode::ATOM), m_Symbol(symbol) {
    }

    char_t m_Symbol;

protected:
    void doPrint(std::ostream & out) const override {
        out << "atom('" << StringUtils::charToString(m_Symbol) << "')" << std::endl;
    }

};

struct EpsilonNode : public SyntaxTreeNode {
    EpsilonNode() : SyntaxTreeNode(SyntaxTreeNode::EPSILON) {
    }

protected:
    void doPrint(std::ostream & out) const override {
        out << "epsilon" << std::endl;
    }
};

struct DotNode : public SyntaxTreeNode {
    DotNode() : SyntaxTreeNode(SyntaxTreeNode::DOT) {
    }

protected:
    void doPrint(std::ostream & out) const override {
        out << "dot" << std::endl;
    }
};

struct CharacterSetNode : public SyntaxTreeNode {
    CharacterSetNode(const CharacterSet & characterSet)
    : SyntaxTreeNode(SyntaxTreeNode::CHAR_SET), m_CharacterSet(characterSet) {
    }

    CharacterSet m_CharacterSet;

protected:
    void doPrint(std::ostream & out) const override {
        out << "character_set(";
        m_CharacterSet.print(out);
        out << ")" << std::endl;
    }
};

struct QuantifierNode : public SyntaxTreeNode {
    QuantifierNode(SyntaxTreeNode::NodeType type, SyntaxTreeNode * inner, bool greedy = true)
        : SyntaxTreeNode(type, inner), m_Greedy(greedy) {
    }

    virtual ~QuantifierNode() = default;

    bool m_Greedy;
};

struct OptionalNode : public QuantifierNode {
    OptionalNode(SyntaxTreeNode * inner, bool greedy = true) : QuantifierNode(SyntaxTreeNode::OPTIONAL, inner, greedy) {
    }

protected:
    void doPrint(std::ostream & out) const override {
        out << "optional" << (m_Greedy ? "" : "(lazy)") << std::endl;
    }
};

struct PosIterNode : public QuantifierNode {
    PosIterNode(SyntaxTreeNode * inner, bool greedy = true) : QuantifierNode(SyntaxTreeNode::POS_ITER, inner, greedy) {
    }

protected:
    void doPrint(std::ostream & out) const override {
        out << "pos_iter" << (m_Greedy ? "" : "(lazy)") << std::endl;
    }
};

struct IterNode : public QuantifierNode {
    IterNode(SyntaxTreeNode * inner, bool greedy = true) : QuantifierNode(SyntaxTreeNode::ITER, inner, greedy) {
    }

protected:
    void doPrint(std::ostream & out) const override {
        out << "iter" << (m_Greedy ? "" : "(lazy)") << std::endl;
    }
};

struct CounterNode : public QuantifierNode {
    CounterNode(SyntaxTreeNode * inner, CountingConstraint countingCn, bool greedy = true) 
        : QuantifierNode(SyntaxTreeNode::COUNTER, inner, greedy), m_CountingCn(countingCn) {
    }

    CountingConstraint m_CountingCn;

    int getNumStates() const override {
        // counting constraint construct requires 2 additional states
        return QuantifierNode::getNumStates() + 2;
    }

protected:
    void doPrint(std::ostream & out) const override {
        out << "counter(";
        m_CountingCn.print(out);
        out << ")" << (m_Greedy ? "" : "(lazy)") << std::endl;
    }
};

struct ConcatNode : public SyntaxTreeNode {
    ConcatNode(SyntaxTreeNode * left, SyntaxTreeNode * right)
    : SyntaxTreeNode(SyntaxTreeNode::CONCAT, left, right) {
    }

protected:
    void doPrint(std::ostream & out) const override {
        out << "concat" << std::endl;
    }
};

struct AlterNode : public SyntaxTreeNode {
    AlterNode(SyntaxTreeNode * left, SyntaxTreeNode * right)
    : SyntaxTreeNode(SyntaxTreeNode::ALTER, left, right) {
    }

protected:
    void doPrint(std::ostream & out) const override {
        out << "alter" << std::endl;
    }
};

struct CaptureNode : public SyntaxTreeNode {
    CaptureNode(SyntaxTreeNode * inner, int captureGroupIndex)
    : SyntaxTreeNode(SyntaxTreeNode::CAPTURE, inner), m_MemIndex(captureGroupIndex - 1) {
    }

    int m_MemIndex;

protected:
    void doPrint(std::ostream & out) const override {
        out << "capture(" << (m_MemIndex + 1) << ")" << std::endl;
    }
};

struct BackrefNode : public SyntaxTreeNode {
    BackrefNode(int captureGroupIndex)
    : SyntaxTreeNode(SyntaxTreeNode::BACKREF), m_MemIndex(captureGroupIndex - 1) {
    }

    int m_MemIndex;

protected:
    void doPrint(std::ostream & out) const override {
        out << "backref(" << (m_MemIndex + 1) << ")" << std::endl;
    }
};

struct StartAnchorNode : public SyntaxTreeNode {
    StartAnchorNode() : SyntaxTreeNode(SyntaxTreeNode::START_ANCHOR) {
    }

protected:
    void doPrint(std::ostream & out) const override {
        out << "^" << std::endl;
    }
};

struct EndAnchorNode : public SyntaxTreeNode {
    EndAnchorNode() : SyntaxTreeNode(SyntaxTreeNode::END_ANCHOR) {
    }

protected:
    void doPrint(std::ostream & out) const override {
        out << "$" << std::endl;
    }
};

struct WordBoundaryNode : public SyntaxTreeNode {
    WordBoundaryNode(bool negated = false) : SyntaxTreeNode(SyntaxTreeNode::WORD_BOUNDARY), m_Negated(negated) {
    }

    bool m_Negated;

protected:
    void doPrint(std::ostream & out) const override {
        out << (m_Negated ? "\\B" : "\\b") << std::endl;
    }
};

} /* namespace mfa */

#endif /* SYNTAX_TREE_H */
