/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef COUNTING_CONSTRAINT_H
#define COUNTING_CONSTRAINT_H

namespace mfa {

/**
 * Representation of a counting constraint in memory automaton.
 */
class CountingConstraint {
public:

    /** Special value representing counting constraint without upper limit on number of repetitions. */
    static const int UNBOUNDED = -1;

    CountingConstraint(int min = 0, int max = 0) : m_Min(min), m_Max(max), m_CounterIdx(0) {
    }

    /**
     * Check if the constraint is satisfied for given value x.
     */
    bool check(int x) const {
        return (x >= m_Min && (isUnbounded() || x <= m_Max));
    }

    /**
     * Check if counter x can be incremented without exceeding the upper bound.
     */
    bool checkInc(int x) const {
        return (isUnbounded() || x < m_Max);
    }

    /**
     * Returns whether this counting constraint has no upper limit on number of repetitions.
     */
    bool isUnbounded() const {
        return (m_Max == UNBOUNDED);
    }

    /**
     * Prints this counting constraint into given stream.
     */
    void print(std::ostream & out) const {
        out << "{";
        out << m_Min;
        if (m_Min != m_Max) {
            out << ",";
            if (m_Max != UNBOUNDED)
                out << m_Max;
        }
        out << "}";
        out << ", idx=" << m_CounterIdx;
    }

    int m_Min;
    int m_Max;
    int m_CounterIdx;
};

} /* namespace mfa */

#endif /* COUNTING_CONSTRAINT_H */
