/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <stdexcept>

namespace mfa {

/**
 * Base class for exceptions thrown by objects from mfa namespace.
 */
class error_base : public ::std::runtime_error {
public:

    error_base(const std::string & title, const std::string & message = "")
        : ::std::runtime_error(title + (!message.empty() ? ": " + message : "")) {
    }
};

/**
 * Invalid regex pattern syntax.
 */
class regex_syntax_error : public error_base {
public:

    regex_syntax_error(const std::string & message = "")
        : error_base("Invalid pattern", message) {
    }
};

/**
 * Memory automaton error (invalid structure or operation).
 */
class automaton_error : public error_base {
public:

    automaton_error(const std::string & message = "")
        : error_base("Automaton error", message) {
    }
};

/**
 * Invalid memory reference error.
 */
class automaton_reference_error : public automaton_error {
public:

    automaton_reference_error(int memIdx)
        : automaton_error("invalid memory reference"), m_MemIdx(memIdx) {
    }

    int m_MemIdx;
};

/**
 * Invalid character set operation error.
 */
class character_set_error : public error_base {
public:

    character_set_error(const std::string & message = "")
        : error_base("Character set error: ", message) {
    }
};

}

#endif /* EXCEPTIONS_H */
