/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef BFS_MATCHER_H
#define BFS_MATCHER_H

#include "matcher.h"
#include "common.h"

#include <queue>
#include <unordered_set>

namespace mfa {

/**
 * Breadth-first search matching algorithm. 
 * 
 * Performs BFS search on space of possible automaton configurations. 
 * If the number of automaton memories is bounded by constant, the asymptotic time 
 * complexity of this algorithm is polynomial. 
 * 
 * @warning The reported capturing groups contents, and even the matched string 
 * in case of search(), may not conform to matching semantics, i.e. greedy and lazy 
 * quantifiers may not strictly behave as defined. The algorithm will find any match. 
 * Therefore this algorithm should be used only for testing if the string matches. 
 * Otherwise, it is recommended to use BacktrackMatcher.
 */
template <typename Automaton>
class BFSMatcher : public Matcher<Automaton> {
public:

    virtual ~BFSMatcher() = default;

    using Configuration = typename Automaton::Configuration;

    virtual bool match(Automaton * automaton, std::string input, MatchResult * result = nullptr) override {
        automaton->setInput(std::move(input));
        return doMatch(automaton, automaton->getInitialConfiguration(), result);
    }

    virtual bool search(Automaton * automaton, std::string input, MatchResult * result = nullptr) override {
        automaton->setInput(std::move(input));
        Configuration initConf(automaton->getInitialConfiguration());

        do {
            if (doMatch(automaton, initConf, result, true))
                return true;
            if (!StringUtils::isAtEnd(initConf.inputIter(), automaton->getInput()))
                ++initConf.inputIter();
        } while (!StringUtils::isAtEnd(initConf.inputIter(), automaton->getInput()));

        if (result != nullptr)
            result->fillFailedMatchResult();
        return false;
    }
    
private:

    bool doMatch(Automaton * automaton, Configuration startConf, MatchResult * result, bool searching = false) const {
        std::queue<Configuration> q;
        std::unordered_set<Configuration, typename Configuration::hash> visited;
        q.push(startConf);
        visited.insert(q.front());
        while (!q.empty()) {
            Configuration conf = std::move(q.front());
            q.pop();
            if (automaton->isAcceptingConfiguration(conf, searching)) {
                if (result != nullptr) {
                    result->fillMatchResult(&conf, automaton->getInput(), startConf.inputIter());
                }
                return true;
            }
            automaton->forEachNextConfiguration(std::move(conf), [&q, &visited] (Configuration nextCnf) {
                if (visited.count(nextCnf) == 0) {
                    visited.insert(nextCnf);
                    q.push(nextCnf);
                }
            });
        }
        if (result != nullptr)
            result->fillFailedMatchResult();
        return false;
    }
};

} /* namespace mfa */

#endif /* BFS_MATCHER_H */
