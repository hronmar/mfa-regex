/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MATCH_RESULT_H
#define MATCH_RESULT_H

#include "common.h"

#include <vector>

namespace mfa {

/**
 * Class for storing match results.
 * 
 * This can be used with the Matcher classes to collect information about the match. 
 */
class MatchResult {
public:
    MatchResult() : m_Ready(false), m_Match(false), m_Position(0) {
    }

    /**
     * Returns if the match results are ready (valid).
     */
    bool isReady() const {
        return m_Ready;
    }

    /**
     * Returns if the match was successful.
     */
    bool matched() const {
        return m_Match;
    }

    /**
     * Returns the position in the input string on which the match starts.
     */
    int getPosition() const {
        return m_Position;
    }

    /**
     * Returns match length.
     */
    int getLength() const {
        return m_MatchedString.size();
    }
    
    /**
     * Returns the matched part of input.
     */
    std::string getMatchedString() const {
        return m_MatchedString;
    }

    /**
     * Returns contents of the capture group with given index. 
     * 
     * Capture group indeces start from 1. If 0 is given, the whole match is returned. 
     */
    std::string operator[](std::size_t index) const {
        if (index == 0)
            return m_MatchedString;
        return m_CaptureGroupContents[index];
    }

    /**
     * Prints information about the match result into given stream. 
     */
    void print(std::ostream & out, bool printCaptureGroups = true) const {
        if (!m_Ready) {
            out << "match results not ready" << std::endl;
            return;
        }
        if (!m_Match) {
            out << "no match" << std::endl;
            return;
        }
        out << "match on position " << m_Position << std::endl;
        out << "matched string: '" << m_MatchedString << "'" << std::endl;
        if (printCaptureGroups) {
            for (size_t i = 1; i < m_CaptureGroupContents.size(); i++)
                out << "capture group " << i << ": '" << m_CaptureGroupContents[i] << "'" << std::endl;
        }
    }

    void fillFailedMatchResult() {
        m_Ready = true;
        m_Match = false;
        m_MatchedString = "";
        m_Position = 0;
        m_CaptureGroupContents.clear();
        return;
    }

    template <typename Configuration>
    void fillMatchResult(Configuration * conf, std::string & input, iterator_t matchStart) {
        m_Ready = true;
        m_Match = true;
        m_Position = StringUtils::getDistance(StringUtils::makeIterator(input), matchStart);
        m_MatchedString = StringUtils::getString(matchStart, conf->inputIter());
        m_CaptureGroupContents.clear();
        m_CaptureGroupContents.reserve(conf->getNumberOfMemories());
        m_CaptureGroupContents.emplace_back(m_MatchedString);
        for (int i = 0; i < conf->getNumberOfMemories(); i++)
            m_CaptureGroupContents.emplace_back(conf->getMemoryContents(i));
    }

private:
    bool m_Ready;
    bool m_Match;
    int m_Position;
    std::string m_MatchedString;
    std::vector<std::string> m_CaptureGroupContents;
};

} /* namespace mfa */

#endif /* MATCH_RESULT_H */
