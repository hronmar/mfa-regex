/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef CHARACTER_SET_H
#define CHARACTER_SET_H

#include "common.h"

#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <ostream>
#include <locale>

namespace mfa {

/**
 * Class implementing character set (or the bracket-expression in regex).
 */
class CharacterSet {
public:

    /**
     * Constructs empty character set.
     * 
     * @param complement Whether this set should behave as complement
     */
    explicit CharacterSet(bool complement = false);
    
    /**
     * Constructs character sets initialized with given characters.
     * 
     * @param complement Whether this set should behave as complement
     * @param characters Initial set of characters (given as std::string)
     */
    CharacterSet(bool complement, const std::string & characters);

    /**
     * Adds single character into the set.
     */
    void addCharacter(char_t character);

    /**
     * Adds inclusive range of characters [from, to] into the set.
     */
    void addRange(char_t from, char_t to);

    /**
     * Adds character class.
     */
    void addCharacterClass(const std::string & className);
    
    /**
     * Returns if the given character is contained in this set.
     */
    bool contains(char_t character) const;

    bool isComplement() const { return m_Complement; }
    void setComplement(bool complement) { m_Complement = complement; }

    void print(std::ostream & out) const;

    bool mergeWith(const CharacterSet & other);

    /**
     * Get character set initialized with given class as its only member.
     */
    static CharacterSet forClass(const std::string & className, bool complement = false);
    
    /**
     * Get character set initialized with given class as its only member, dynamically allocated.
     */
    static CharacterSet * newForClass(const std::string & className, bool complement = false);
    
private:

    bool m_Complement;
    std::unordered_set<char_t> m_Characters;

    struct Range {
        char_t m_From;
        char_t m_To;

        Range(char_t from, char_t to);
        bool contains(char_t character) const;
    };

    std::vector<Range> m_CharRanges;

    std::ctype_base::mask m_CharClassesMask;

    static const std::unordered_map<std::string, std::ctype_base::mask> m_CharClassNameToMaskMap;
};

} /* namespace mfa */

#endif /* CHARACTER_SET_H */
