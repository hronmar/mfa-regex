/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "common.h"

namespace mfa {

#ifdef UNICODE_SUPPORT

iterator_t StringUtils::makeIterator(std::string & str) {
    return iterator_t(str.begin(), str.begin(), str.end());
}

bool StringUtils::isAtStart(const iterator_t & iter, const std::string & str) {
    return (iter.base() == str.begin());
}

bool StringUtils::isAtEnd(const iterator_t & iter, const std::string & str) {
    return (iter.base() == str.end());
}

int StringUtils::getDistance(const iterator_t & begin, const iterator_t & end) {
    return utf8::distance(begin.base(), end.base());
}

std::string StringUtils::getString(const iterator_t & begin, const iterator_t & end) {
    return std::string(begin.base(), end.base());
}

std::string StringUtils::charToString(char_t chr) {
    return utf8::utf32to8(std::u32string{chr});
}

size_t StringUtils::hashIter(const iterator_t & iter, const std::string & str) {
    std::hash<const char *> hasher;
    return isAtEnd(iter, str)
        ? hasher(nullptr)
        : hasher(&*iter.base());
}

void StringUtils::append(char_t character, std::string & string) {
    utf8::append((char32_t) character, string);
}

bool StringUtils::testCharacterClass(char_t character, std::ctype_base::mask classMask) {
    static std::locale loc;
    return std::use_facet<std::ctype<wchar_t>>(loc).is(classMask, character);
}

#else

iterator_t StringUtils::makeIterator(std::string & str) {
    return str.data();
}

bool StringUtils::isAtStart(const iterator_t & iter, const std::string & str) {
    return (iter == str.data());
}

bool StringUtils::isAtEnd(const iterator_t & iter, const std::string & str) {
    return (iter - str.data() >= (int) str.size());
}

int StringUtils::getDistance(const iterator_t & begin, const iterator_t & end) {
    return (end - begin);
}

std::string StringUtils::getString(const iterator_t & begin, const iterator_t & end) {
    return std::string(begin, end);
}

std::string StringUtils::charToString(char_t chr) {
    return std::string(1, chr);
}

size_t StringUtils::hashIter(const iterator_t & iter, const std::string & str) {
    return std::hash<iterator_t>()(iter);
}

void StringUtils::append(char_t character, std::string & string) {
    string += character;
}

bool StringUtils::testCharacterClass(char_t character, std::ctype_base::mask classMask) {
    static std::locale loc;
    return std::use_facet<std::ctype<char>>(loc).is(classMask, character);
}

#endif

} /* namespace mfa */
