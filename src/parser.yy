/**
 * Implementation of regex parser.
 * 
 * Grammar based on: https://swtch.com/~rsc/regexp/nfa-perl.y.txt
 */

%skeleton "lalr1.cc" /* -*- C++ -*- */
%require "3.2"
%language "c++"

%defines "parser.h"
%output "parser.cpp"

%define api.namespace {mfa}

%code requires {
    #include "common.h"
    #include "exceptions.h"
    #include "syntax_tree.h"
    #include "mfa_builder.h"
    #include "character_set.h"
    #include "counting_constraint.h"

    #include <cctype>
    #include <string>

    // forward declarations
    namespace mfa { class Lexer; }
}

%code {
    #include "lexer.h"
}

%param { Lexer * lexer }
%parse-param { MFA_Builder * bld }

%define api.value.type variant

%printer { yyo << $$; } <*>;

%define api.token.constructor

%define api.token.prefix {TOKEN_}
%token
    END  0     "end of input"
    VBAR       "|"
    DOT        "."
    STAR       "*"
    PLUS       "+"
    QUESTION_M "?"
    CARET      "^"
    DOLLAR     "$"
    LPAREN     "("
    RPAREN     ")"
    NONCAP_LPAR "(?:"
    NONCAP_LPAR_RESET "(?|"
;

%token <char_t> SYMBOL "symbol"
%token <int> BACKREF "backref"
%token <int> RELATIVE_BACKREF "relative_backref"
%token <CharacterSet> BRACKET_EXPR "bracket_expr"
%token <bool> WORD_BOUNDARY_ASSERT "word_boundary"
%token <CountingConstraint> COUNTER "counter"
%token <std::string> NAMED_CAPTURE "named_capture"
%token <std::string> NAMED_REDEFINITION "named_redefinition"
%token <std::string> NAMED_BACKREF "named_backref"

%type <SyntaxTreeNode *> alt cgresetalt concat repeat term
%type <int>          cgindex namedcgdef namedcgredef
%%
%start regex;

regex: alt                 { bld->buildResult($1); }

alt:
    concat                 { $$ = $1; }
|   alt "|" concat         { $$ = new AlterNode($1, $3); }
|   /* empty */            { $$ = new EpsilonNode(); }
|   alt "|"                { $$ = new OptionalNode($1); }
;

concat:
    repeat                 { $$ = $1; }
|   concat repeat          { $$ = new ConcatNode($1, $2); }
;

repeat:
    term                   { $$ = $1; }
|   term "+"               { $$ = new PosIterNode($1); }
|   term "*"               { $$ = new IterNode($1); }
|   term "?"               { $$ = new OptionalNode($1); }
|   term "+" "?"           { $$ = new PosIterNode($1, false); }
|   term "*" "?"           { $$ = new IterNode($1, false); }
|   term "?" "?"           { $$ = new OptionalNode($1, false); }
|   term COUNTER           { $$ = new CounterNode($1, $2); }
|   term COUNTER "?"       { $$ = new CounterNode($1, $2, false); }
;

cgresetalt:
    concat                 { $$ = $1; }
|   cgresetalt "|" restorecgindex concat { $$ = new AlterNode($1, $4); }
|   /* empty */            { $$ = new EpsilonNode(); }
|   cgresetalt "|"         { $$ = new OptionalNode($1); }
;

term:
    "(" cgindex alt ")"    { $$ = new CaptureNode($3, $2); }
|   namedcgdef alt ")"     { $$ = new CaptureNode($2, $1); }
|   namedcgredef alt ")"   { $$ = new CaptureNode($2, $1); }
|   "(?:" alt ")"          { $$ = $2; }
|   "(?|" savecgindex cgresetalt ")" { $$ = $3; bld->popCGIndex(); }
|   SYMBOL                 { $$ = new AtomNode($1); }
|   "."                    { $$ = new DotNode(); }
|   "^"                    { $$ = new StartAnchorNode(); }
|   "$"                    { $$ = new EndAnchorNode(); }
|   WORD_BOUNDARY_ASSERT   { $$ = new WordBoundaryNode($1); }
|   BACKREF                { $$ = new BackrefNode($1); }
|   RELATIVE_BACKREF       { $$ = new BackrefNode(bld->peekNextCGIndex() + $1); }
|   NAMED_BACKREF          { $$ = bld->makeNamedBackref($1); }
|   BRACKET_EXPR           { $$ = new CharacterSetNode($1); }
;

namedcgdef:
    NAMED_CAPTURE cgindex  { $$ = bld->defineNamedCaptureGroup($1, $2); };
    
namedcgredef:
    NAMED_REDEFINITION     { $$ = bld->getNamedCaptureGroup($1, true); };

cgindex:
    /* empty */            { $$ = bld->nextCGIndex(); };

savecgindex:
    /* empty */            { bld->pushCGIndex(); };

restorecgindex:
    /* empty */            { bld->restoreCGIndex(); };
%%
namespace mfa {   
    // error handling
    auto parser::error(const std::string & msg) -> void {
      throw regex_syntax_error(msg);
    }
}
