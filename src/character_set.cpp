/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "character_set.h"
#include "exceptions.h"

namespace mfa {

CharacterSet::CharacterSet(bool complement) : m_Complement(complement),
    m_CharClassesMask(0) {
}

CharacterSet::CharacterSet(bool complement, const std::string & characters) : CharacterSet(complement) {
    for (char c : characters)
        addCharacter(c);
}

void CharacterSet::addCharacter(char_t character) {
    m_Characters.insert(character);
}

void CharacterSet::addRange(char_t from, char_t to) {
    m_CharRanges.emplace_back(from, to);
}

void CharacterSet::addCharacterClass(const std::string & className) {
    if (className == "word") {
        // character class 'word' is not in std::locale (it is also not in POSIX),
        // here we treat [[:word:]] as equivalent to "[[:alnum:]_]"
        addCharacterClass("alnum");
        addCharacter('_');
    } else {
        if (m_CharClassNameToMaskMap.count(className) == 0) {
            throw character_set_error("Invalid character class");
        }
        m_CharClassesMask |= m_CharClassNameToMaskMap.at(className);
    }
}

const std::unordered_map<std::string, std::ctype_base::mask> CharacterSet::m_CharClassNameToMaskMap = {
    {"alnum", std::ctype_base::alnum},
    {"alpha", std::ctype_base::alpha},
    {"blank", std::ctype_base::blank},
    {"cntrl", std::ctype_base::cntrl},
    {"digit", std::ctype_base::digit},
    {"graph", std::ctype_base::graph},
    {"lower", std::ctype_base::lower},
    {"print", std::ctype_base::print},
    {"punct", std::ctype_base::punct},
    {"space", std::ctype_base::space},
    {"upper", std::ctype_base::upper},
    {"xdigit", std::ctype_base::xdigit}
};

bool CharacterSet::contains(char_t character) const {
    if (m_Characters.count(character) > 0)
        return !m_Complement;
    for (const Range & range : m_CharRanges)
        if (range.contains(character))
            return !m_Complement;
    if (m_CharClassesMask != 0) {
        if (StringUtils::testCharacterClass(character, m_CharClassesMask))
            return !m_Complement;
    }
    return m_Complement;
}

void CharacterSet::print(std::ostream & out) const {
    out << "[";
    if (m_Complement)
        out << "^";
    for (char_t ch : m_Characters)
        out << StringUtils::charToString(ch);
    for (const Range & r : m_CharRanges)
        out << StringUtils::charToString(r.m_From)
        << "-"
        << StringUtils::charToString(r.m_To);
    if (m_CharClassesMask != 0) {
        std::ctype_base::mask mask = m_CharClassesMask;
        for (const auto & charClass : m_CharClassNameToMaskMap)
            if ((mask & charClass.second) == charClass.second) {
                out << "[:" << charClass.first << ":]";
                mask ^= charClass.second;
            }
    }
    out << "]";
}

bool CharacterSet::mergeWith(const CharacterSet & other) {
    if (m_Complement || other.m_Complement)
        return false;

    m_CharClassesMask |= other.m_CharClassesMask;
    m_CharRanges.reserve(m_CharRanges.size() + other.m_CharRanges.size());
    m_CharRanges.insert(std::end(m_CharRanges), std::begin(other.m_CharRanges), std::end(other.m_CharRanges));
    for (char_t character : other.m_Characters)
        if (contains(character) == m_Complement)
            m_Characters.insert(character);

    return true;
}

CharacterSet CharacterSet::forClass(const std::string & className, bool complement) {
    CharacterSet charSet(complement);
    charSet.addCharacterClass(className);
    return charSet;
}

CharacterSet * CharacterSet::newForClass(const std::string & className, bool complement) {
    return new CharacterSet(forClass(className, complement));
}

CharacterSet::Range::Range(char_t from, char_t to) : m_From(from), m_To(to) {
}

bool CharacterSet::Range::contains(char_t character) const {
    return (character >= m_From && character <= m_To);
}

} /* namespace mfa */
