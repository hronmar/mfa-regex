/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "common.h"
#include "avd_optimized_mfa.h"
#include "memory_automaton.h"
#include "mfa_builder.h"
#include "backtrack_matcher.h"
#include "exceptions.h"

#include <cstdlib>
#include <iostream>
#include <memory>

using namespace mfa;

int main(int argc, char ** argv) {
    std::string pattern, text;

    // set preferred system locale as current locale
    std::locale::global(std::locale(""));

    std::cout << "pattern: ";
    std::getline(std::cin, pattern);

    using Automaton = MemoryAutomaton;

    std::unique_ptr<Automaton> mfa;

    Parameters params;
    params.m_UnusedMemoriesOptEn = false;
    params.m_EnablePatternOptimizations = false;
    try {
        mfa = std::make_unique<Automaton>(pattern, params);
    } catch (regex_syntax_error & ex) {
        std::cerr << ex.what() << std::endl;
        return EXIT_FAILURE;
    } catch (std::exception & ex) {
        std::cerr << "Error during pattern construction: " << ex.what() << std::endl;
        return EXIT_FAILURE;
    }

    BacktrackMatcher<Automaton> matcher;

    while (true) {
        std::cout << "input: ";
        if (!std::getline(std::cin, text))
            break;

        MatchResult m;
        try {
            matcher.search(mfa.get(), text, &m);
            m.print(std::cout);
            std::cout << std::endl;
        } catch (std::exception & ex) {
            std::cerr << "Error: " << ex.what() << std::endl;
            return EXIT_FAILURE;
        }
    }

    std::cout << std::endl << std::endl << "syntax tree:" << std::endl;
    mfa->getSyntaxTree()->print(std::cout);

    return EXIT_SUCCESS;
}
