/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "avd_optimized_mfa.h"
#include "backtrack_matcher.h"
#include "exceptions.h"

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>

using namespace mfa;

static void printUsage() {
    std::cout << "Usage: mfa-grep PATTERN [FILE]..." << std::endl;
}

int main(int argc, char ** argv) {
    using Automaton = AvdOptimizedMFA;

    // set preferred system locale as current locale
    std::locale::global(std::locale(""));

    if (argc < 2) {
        std::cerr << "Too few arguments given." << std::endl;
        printUsage();
        return EXIT_FAILURE;
    }

    std::string pattern(argv[1]);
    std::unique_ptr<Automaton> mfa;

    try {
        mfa = std::make_unique<Automaton>(pattern);
    } catch (regex_syntax_error & ex) {
        std::cerr << ex.what() << std::endl;
        return EXIT_FAILURE;
    } catch (std::exception & ex) {
        std::cerr << "Error during pattern construction: " << ex.what() << std::endl;
        return EXIT_FAILURE;
    }

    BacktrackMatcher<Automaton> matcher;
    std::string line;
    
    if (argc >= 3) {
        // at least one file given
        for (int i = 2; i < argc; i++) {
            std::ifstream in(argv[i]);
            if (!in.is_open()) {
                std::cerr << "Warning: Could not open input file: " << argv[i] << std::endl;
                continue;
            }

            while (std::getline(in, line)) {
                if (matcher.search(mfa.get(), line))
                    std::cout << line << std::endl;
            }
        }
    } else {
        // read from standard input
        while (std::getline(std::cin, line)) {
            if (matcher.search(mfa.get(), line))
                std::cout << line << std::endl;
        }
    }

    return EXIT_SUCCESS;
}
