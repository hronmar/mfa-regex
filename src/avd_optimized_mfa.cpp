/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "avd_optimized_mfa.h"

namespace mfa {

using Configuration = AvdOptimizedMFA::Configuration;

AvdOptimizedMFA::AvdOptimizedMFA(std::unique_ptr<MemoryAutomaton> automaton) : m_Wrappee(std::move(automaton)) {
    finalize();
}

AvdOptimizedMFA::AvdOptimizedMFA(const std::string & pattern, Parameters params)
    : AvdOptimizedMFA(std::make_unique<MemoryAutomaton>(pattern, params)) {
}

Parameters AvdOptimizedMFA::getParameters() const {
    return m_Wrappee->getParameters();
}

void AvdOptimizedMFA::prepareStates(int numStates) {
    m_Wrappee->prepareStates(numStates);
}

AvdOptimizedMFA::State * AvdOptimizedMFA::addState() {
    return m_Wrappee->addState();
}

void AvdOptimizedMFA::addTransition(Transition tr, State * from, State * to, bool lazy) {
    m_Wrappee->addTransition(tr, from, to, lazy);
}

void AvdOptimizedMFA::addMemory() {
    m_Wrappee->addMemory();
}

void AvdOptimizedMFA::setInitialState(State * state) {
    m_Wrappee->setInitialState(state);
}

void AvdOptimizedMFA::setFinalState(State * state) {
    m_Wrappee->setFinalState(state);
}

void AvdOptimizedMFA::finalize() {
    // first remove unused memories
    m_Wrappee->m_Parameters.m_UnusedMemoriesOptEn = true;
    m_Wrappee->finalize();
    m_OrigNumVaribles = m_Wrappee->m_NumMemories;

    // compute active variable sets (and active variable degree)
    m_ActiveVariableDegree = mfa::computeActiveVariables(m_Wrappee.get(), m_ActiveVariableSets);
    m_Wrappee->m_NumMemories = m_Wrappee->m_NumUsedMemories = m_ActiveVariableDegree;
}

int AvdOptimizedMFA::getNumMemories() const {
    return m_Wrappee->getNumMemories();
}

int AvdOptimizedMFA::getActiveVariableDegree() const {
    return m_ActiveVariableDegree;
}

void AvdOptimizedMFA::setSyntaxTree(SyntaxTreeNode * syntaxTree) {
    m_Wrappee->setSyntaxTree(syntaxTree);
}

SyntaxTreeNode * AvdOptimizedMFA::getSyntaxTree() const {
    return m_Wrappee->getSyntaxTree();
}

void AvdOptimizedMFA::setInput(const std::string & str) {
    m_Wrappee->setInput(str);
}

void AvdOptimizedMFA::setInput(std::string && str) {
    m_Wrappee->setInput(std::move(str));
}

std::string & AvdOptimizedMFA::getInput() {
    return m_Wrappee->getInput();
}

Configuration AvdOptimizedMFA::getInitialConfiguration() {
    return Configuration(m_Wrappee->getInitialConfiguration(), m_ActiveVariableDegree, m_OrigNumVaribles);
}

bool AvdOptimizedMFA::isAcceptingConfiguration(const Configuration & conf, bool searching) const {
    return m_Wrappee->isAcceptingConfiguration(conf.m_OrigConf, searching);
}

std::vector<Configuration> AvdOptimizedMFA::getNextConfigurations(const Configuration & conf) const {
    std::vector<Configuration> nextConfs;
    forEachNextConfiguration(conf,
        [&nextConfs](Configuration c) {
            nextConfs.push_back(std::move(c));
        });
    return nextConfs;
}

bool AvdOptimizedMFA::executeTransition(Configuration & conf, const Transition & tr, State * to) const {
    return m_Wrappee->executeTransition(conf.m_OrigConf, tr, to);
}

} /* namespace mfa */
