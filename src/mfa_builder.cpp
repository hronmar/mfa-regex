/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "mfa_builder.h"
#include "memory_automaton.h"
#include "lexer.h"
#include "parser.h"

namespace mfa {

using Transition = typename MemoryAutomaton::Transition;

MFA_Builder::MFA_Builder()
    : m_NextCGIndex(FIRST_CG_INDEX), m_Automaton(nullptr) {
}

void MFA_Builder::buildInto(const std::string & pattern, MemoryAutomaton * automaton) {
    m_Pattern = pattern;
    m_Automaton = automaton;

    parse();

    try {
        m_Automaton->finalize();
    } catch (const automaton_reference_error & e) {
        throw regex_syntax_error("Backreference to non-existing capture group number " + std::to_string(e.m_MemIdx + 1));
    } catch (const automaton_error & e) {
        throw regex_syntax_error(e.what());
    }
    m_Automaton = nullptr;
}

std::unique_ptr<MemoryAutomaton> MFA_Builder::build(const std::string & pattern, Parameters params) {
    std::unique_ptr<MemoryAutomaton> automaton = std::make_unique<MemoryAutomaton>(params);
    buildInto(pattern, automaton.get());
    return automaton;
}

bool MFA_Builder::parse() {
    resetCtx();
    Lexer lexer(m_Pattern);
    parser mfaParser(&lexer, this);
    int ret = mfaParser();
    resolveNamedCaptures();
    return (ret == 0);
}

int MFA_Builder::nextCGIndex() {
    if (m_Automaton->getNumMemories() < m_NextCGIndex)
        m_Automaton->addMemory();
    return m_NextCGIndex++;
}

void MFA_Builder::resetCGIndex() {
    m_NextCGIndex = FIRST_CG_INDEX;
}

void MFA_Builder::pushCGIndex() {
    m_StoredCGIndeces.push(m_NextCGIndex);
}

void MFA_Builder::popCGIndex() {
    m_StoredCGIndeces.pop();
}

void MFA_Builder::restoreCGIndex() {
    m_NextCGIndex = m_StoredCGIndeces.top();
}

int MFA_Builder::peekNextCGIndex() const {
    return m_NextCGIndex;
}

void MFA_Builder::resetCtx() {
    resetCGIndex();
    m_NamedCaptures.clear();
    m_NamedCapturesIndeces.clear();
    m_UnresolvedCaptures.clear();
}

MFA_Fragment MFA_Builder::compile(AtomNode * node) {
    MFA_Fragment fragment(m_Automaton, node);
    m_Automaton->addTransition(Transition(Transition::SYMBOL, node->m_Symbol), fragment.Tin(), fragment.Tout());
    return fragment;
}

MFA_Fragment MFA_Builder::compile(PosIterNode * node) {
    MFA_Fragment inner(compile(node->m_Left));
    MFA_Fragment fragment(m_Automaton, node, inner);
    m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tin(), inner.Tin());
    m_Automaton->addTransition(Transition(Transition::EPSILON), inner.Tout(), fragment.Tout());
    m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tout(), fragment.Tin(), !node->m_Greedy);
    return fragment;
}

MFA_Fragment MFA_Builder::compile(ConcatNode * node) {
    MFA_Fragment left(compile(node->m_Left));
    MFA_Fragment right(compile(node->m_Right));
    MFA_Fragment fragment(m_Automaton, node, left, right);
    m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tin(), left.Tin());
    m_Automaton->addTransition(Transition(Transition::EPSILON), left.Tout(), right.Tin());
    m_Automaton->addTransition(Transition(Transition::EPSILON), right.Tout(), fragment.Tout());
    return fragment;
}

MFA_Fragment MFA_Builder::compile(AlterNode * node) {
    MFA_Fragment left(compile(node->m_Left));
    MFA_Fragment right(compile(node->m_Right));
    MFA_Fragment fragment(m_Automaton, node, left, right);
    m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tin(), left.Tin());
    m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tin(), right.Tin());
    m_Automaton->addTransition(Transition(Transition::EPSILON), left.Tout(), fragment.Tout());
    m_Automaton->addTransition(Transition(Transition::EPSILON), right.Tout(), fragment.Tout());
    return fragment;
}

MFA_Fragment MFA_Builder::compile(CaptureNode * node) {
    MFA_Fragment inner(compile(node->m_Left));
    if (inner.m_Var.count(node->m_MemIndex) > 0)
        throw regex_syntax_error("backreference to enclosing capture group not allowed");
    MFA_Fragment fragment(m_Automaton, node, inner);
    fragment.m_Var.insert(node->m_MemIndex);
    m_Automaton->addTransition(Transition(Transition::MEMORY_OPEN, node->m_MemIndex), fragment.Tin(), inner.Tin());
    m_Automaton->addTransition(Transition(Transition::MEMORY_CLOSE, node->m_MemIndex), inner.Tout(), fragment.Tout());
    return fragment;
}

MFA_Fragment MFA_Builder::compile(BackrefNode * node) {
    MFA_Fragment fragment(m_Automaton, node);
    fragment.m_Var.insert(node->m_MemIndex);
    m_Automaton->addTransition(Transition(Transition::MEMORY_RECALL, node->m_MemIndex), fragment.Tin(), fragment.Tout());
    return fragment;
}

MFA_Fragment MFA_Builder::compile(EpsilonNode * node) {
    MFA_Fragment fragment(m_Automaton, node);
    m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tin(), fragment.Tout());
    return fragment;
}

MFA_Fragment MFA_Builder::compile(OptionalNode * node) {
    MFA_Fragment inner(compile(node->m_Left));
    MFA_Fragment fragment(m_Automaton, node, inner);
    m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tin(), inner.Tin(), !node->m_Greedy);
    m_Automaton->addTransition(Transition(Transition::EPSILON), inner.Tout(), fragment.Tout());
    m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tin(), fragment.Tout());
    return fragment;
}

MFA_Fragment MFA_Builder::compile(IterNode * node) {
    MFA_Fragment inner(compile(node->m_Left));
    MFA_Fragment fragment(m_Automaton, node, inner);
    m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tin(), inner.Tin(), !node->m_Greedy);
    m_Automaton->addTransition(Transition(Transition::EPSILON), inner.Tout(), fragment.Tin());
    m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tin(), fragment.Tout());
    return fragment;
}

MFA_Fragment MFA_Builder::compile(DotNode * node) {
    MFA_Fragment fragment(m_Automaton, node);
    m_Automaton->addTransition(Transition(Transition::DOT), fragment.Tin(), fragment.Tout());
    return fragment;
}

MFA_Fragment MFA_Builder::compile(CharacterSetNode * node) {
    MFA_Fragment fragment(m_Automaton, node);
    m_Automaton->addTransition(Transition(Transition::CHARACTER_SET, new CharacterSet(node->m_CharacterSet)), fragment.Tin(), fragment.Tout());
    return fragment;
}

MFA_Fragment MFA_Builder::compile(CounterNode * node) {
    CountingConstraint & cnt = node->m_CountingCn;

    MFA_Fragment inner(compile(node->m_Left));
    MFA_Fragment fragment(m_Automaton, node, inner);
    cnt.m_CounterIdx = fragment.m_CounterIdx++;

    MemoryAutomaton::State * stateInc = m_Automaton->addState();
    MemoryAutomaton::State * stateOut = m_Automaton->addState();

    if (node->m_Greedy) {
        m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tin(), stateInc);
        m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tin(), stateOut);
    } else {
        m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tin(), stateOut);
        m_Automaton->addTransition(Transition(Transition::EPSILON), fragment.Tin(), stateInc);
    }

    m_Automaton->addTransition(Transition(Transition::COUNTER_INC, cnt), stateInc, inner.Tin());
    m_Automaton->addTransition(Transition(Transition::EPSILON), inner.Tout(), fragment.Tin());
    
    m_Automaton->addTransition(Transition(Transition::COUNTER_OUT, cnt), stateOut, fragment.Tout());

    return fragment;
}

MFA_Fragment MFA_Builder::compile(StartAnchorNode * node) {
    MFA_Fragment fragment(m_Automaton, node);
    m_Automaton->addTransition(Transition(Transition::START_ANCHOR), fragment.Tin(), fragment.Tout());
    return fragment;
}

MFA_Fragment MFA_Builder::compile(EndAnchorNode * node) {
    MFA_Fragment fragment(m_Automaton, node);
    m_Automaton->addTransition(Transition(Transition::END_ANCHOR), fragment.Tin(), fragment.Tout());
    return fragment;
}

MFA_Fragment MFA_Builder::compile(WordBoundaryNode * node) {
    MFA_Fragment fragment(m_Automaton, node);
    m_Automaton->addTransition(Transition(node->m_Negated ? Transition::NOT_WORD_BOUNDARY : Transition::WORD_BOUNDARY), 
        fragment.Tin(), fragment.Tout());
    return fragment;
}

MFA_Fragment MFA_Builder::compile(SyntaxTreeNode * node) {
    switch (node->m_NodeType) {
        case SyntaxTreeNode::ALTER:
            return compile(static_cast<AlterNode *>(node));
        case SyntaxTreeNode::ATOM:
            return compile(static_cast<AtomNode *>(node));
        case SyntaxTreeNode::BACKREF:
            return compile(static_cast<BackrefNode *>(node));
        case SyntaxTreeNode::CAPTURE:
            return compile(static_cast<CaptureNode *>(node));
        case SyntaxTreeNode::CHAR_SET:
            return compile(static_cast<CharacterSetNode *>(node));
        case SyntaxTreeNode::CONCAT:
            return compile(static_cast<ConcatNode *>(node));
        case SyntaxTreeNode::COUNTER:
            return compile(static_cast<CounterNode *>(node));
        case SyntaxTreeNode::DOT:
            return compile(static_cast<DotNode *>(node));
        case SyntaxTreeNode::END_ANCHOR:
            return compile(static_cast<EndAnchorNode *>(node));
        case SyntaxTreeNode::EPSILON:
            return compile(static_cast<EpsilonNode *>(node));
        case SyntaxTreeNode::ITER:
            return compile(static_cast<IterNode *>(node));
        case SyntaxTreeNode::OPTIONAL:
            return compile(static_cast<OptionalNode *>(node));
        case SyntaxTreeNode::POS_ITER:
            return compile(static_cast<PosIterNode *>(node));
        case SyntaxTreeNode::START_ANCHOR:
            return compile(static_cast<StartAnchorNode *>(node));
        case SyntaxTreeNode::WORD_BOUNDARY:
            return compile(static_cast<WordBoundaryNode *>(node));
    }
    throw std::logic_error("Type not handled in switch");
}

SyntaxTreeNode * MFA_Builder::optimize(SyntaxTreeNode * node) {
    // first optimize sub-patterns
    if (node->m_Left != nullptr) {
        node->m_Left = optimize(node->m_Left);
        if (node->m_Right != nullptr)
            node->m_Right = optimize(node->m_Right);
    }

    SyntaxTreeNode * result = node;
    switch (node->m_NodeType) {
        case SyntaxTreeNode::ALTER:
        {
            if (node->m_Left->m_NodeType == SyntaxTreeNode::ATOM && node->m_Right->m_NodeType == SyntaxTreeNode::ATOM) {
                // a|b -> [ab]
                CharacterSet charSet;
                charSet.addCharacter(static_cast<AtomNode *>(node->m_Left)->m_Symbol);
                charSet.addCharacter(static_cast<AtomNode *>(node->m_Right)->m_Symbol);
                delete node;
                return new CharacterSetNode(charSet);
            } else if ((node->m_Left->m_NodeType == SyntaxTreeNode::CHAR_SET && !static_cast<CharacterSetNode *>(node->m_Left)->m_CharacterSet.isComplement())
                && node->m_Right->m_NodeType == SyntaxTreeNode::ATOM) {
                // [a...]|c -> [a...c]
                static_cast<CharacterSetNode *>(node->m_Left)->m_CharacterSet.addCharacter(static_cast<AtomNode *>(node->m_Right)->m_Symbol);
                result = node->m_Left;
                node->m_Left = nullptr;
                delete node;
            } else if (node->m_Left->m_NodeType == SyntaxTreeNode::ATOM
                && (node->m_Right->m_NodeType == SyntaxTreeNode::CHAR_SET && !static_cast<CharacterSetNode *>(node->m_Right)->m_CharacterSet.isComplement())) {
                // convert to previous case
                std::swap(node->m_Left, node->m_Right);
                return optimize(node);
            } else if (node->m_Left->m_NodeType == SyntaxTreeNode::CHAR_SET && node->m_Right->m_NodeType == SyntaxTreeNode::CHAR_SET) {
                // [a...][b...] -> [ab...]
                if (static_cast<CharacterSetNode *>(node->m_Left)->m_CharacterSet.mergeWith(static_cast<CharacterSetNode *>(node->m_Right)->m_CharacterSet)) {
                    result = node->m_Left;
                    node->m_Left = nullptr;
                    delete node;
                }
            } else if (node->m_Left->m_NodeType == SyntaxTreeNode::DOT || node->m_Right->m_NodeType == SyntaxTreeNode::DOT) {
                // .|a -> . or .|[...] -> . or .|. -> .
                SyntaxTreeNode * other = (node->m_Left->m_NodeType == SyntaxTreeNode::DOT ? node->m_Right : node->m_Left);
                if (other->m_NodeType == SyntaxTreeNode::ATOM || other->m_NodeType == SyntaxTreeNode::CHAR_SET
                    || other->m_NodeType == SyntaxTreeNode::DOT) {
                    delete node;
                    return new DotNode();
                }
            }
            break;
        }
        case SyntaxTreeNode::COUNTER:
        {
            CountingConstraint & cnt = static_cast<CounterNode *>(node)->m_CountingCn;
            const bool greedy = static_cast<CounterNode *>(node)->m_Greedy;

            if (node->m_Left->m_NodeType == SyntaxTreeNode::COUNTER) {
                const CountingConstraint & cntInner = static_cast<CounterNode *>(node->m_Left)->m_CountingCn;
                if ((cnt.m_Min == cnt.m_Max && cntInner.m_Min == cntInner.m_Max)) {
                    // (?:x{a}){b} -> x{a*b}
                    cnt.m_Min *= cntInner.m_Min;
                    cnt.m_Max = cnt.m_Min;
                    SyntaxTreeNode * innermost = node->m_Left->m_Left;
                    node->m_Left->m_Left = nullptr;
                    delete node->m_Left;
                    node->m_Left = innermost;
                }
            }

            if (cnt.m_Min == 0 && cnt.m_Max == 0) {
                // x{0} -> epsilon
                delete node;
                return new EpsilonNode();
            } else if (cnt.m_Min == 1 && cnt.m_Max == 1) {
                // x{1} -> a
                result = node->m_Left;
                node->m_Left = nullptr;
                delete node;
            } else if (cnt.m_Min == 0 && cnt.m_Max == 1) {
                // x{0,1} -> a?
                result = new OptionalNode(node->m_Left, greedy);
                node->m_Left = nullptr;
                delete node;
            } else if (cnt.m_Max == CountingConstraint::UNBOUNDED) {
                if (cnt.m_Min == 0) {
                    // x{0,} -> x*
                    result = new IterNode(node->m_Left, greedy);
                    node->m_Left = nullptr;
                    delete node;
                } else if (cnt.m_Min == 1) {
                    // x{1,} -> x+
                    result = new PosIterNode(node->m_Left, greedy);
                    node->m_Left = nullptr;
                    delete node;
                }
            }
            break;
        }
        case SyntaxTreeNode::ITER:
            if ((node->m_Left->m_NodeType == SyntaxTreeNode::ITER
                || node->m_Left->m_NodeType == SyntaxTreeNode::POS_ITER
                || node->m_Left->m_NodeType == SyntaxTreeNode::OPTIONAL)
                && static_cast<QuantifierNode *>(node->m_Left)->m_Greedy == static_cast<QuantifierNode *>(node)->m_Greedy) {
                // x** | x+* | x?* -> x*
                SyntaxTreeNode * inner = node->m_Left->m_Left;
                node->m_Left->m_Left = nullptr;
                delete node->m_Left;
                node->m_Left = inner;
            }
            break;
        case SyntaxTreeNode::POS_ITER:
        case SyntaxTreeNode::OPTIONAL:
            if ((node->m_Left->m_NodeType == SyntaxTreeNode::ITER
                || node->m_Left->m_NodeType == SyntaxTreeNode::POS_ITER
                || node->m_Left->m_NodeType == SyntaxTreeNode::OPTIONAL)
                && static_cast<QuantifierNode *>(node->m_Left)->m_Greedy == static_cast<QuantifierNode *>(node)->m_Greedy) {
                if (node->m_Left->m_NodeType == node->m_NodeType) {
                    // x++ -> x+ or x?? -> x?
                    SyntaxTreeNode * inner = node->m_Left->m_Left;
                    node->m_Left->m_Left = nullptr;
                    delete node->m_Left;
                    node->m_Left = inner;
                } else {
                    // x*+ | x?+ -> x* or x*? | x+? -> x*
                    SyntaxTreeNode * inner = node->m_Left->m_Left;
                    node->m_Left->m_Left = nullptr;
                    result = new IterNode(inner, static_cast<QuantifierNode *>(node)->m_Greedy);
                    delete node;
                }
            }
            break;
        default:
            break;
    }

    return result;
}

void MFA_Builder::buildResult(SyntaxTreeNode * node) {
    if (m_Automaton->getParameters().m_EnablePatternOptimizations) {
        node = optimize(node);
    }

    m_Automaton->prepareStates(node->getNumStates() + 2);

    MFA_Fragment result(compile(node));

    MemoryAutomaton::State * qIn = m_Automaton->addState();
    MemoryAutomaton::State * qF = m_Automaton->addState();
    m_Automaton->addTransition(Transition(Transition::EPSILON), qIn, result.Tin());
    m_Automaton->addTransition(Transition(Transition::EPSILON), result.Tout(), qF);
    m_Automaton->setInitialState(qIn);
    m_Automaton->setFinalState(qF);
    m_Automaton->setSyntaxTree(result.m_Node);
}

int MFA_Builder::defineNamedCaptureGroup(const std::string & name, int k) {
    auto it = m_NamedCaptures.find(name);
    if (it != m_NamedCaptures.end() && it->second != k) {
        throw regex_syntax_error("capture group name must be unique, use (?<&" + name + ">...) for redefinition");
    }
    if (m_NamedCapturesIndeces.count(k) != 0) {
        // capture group k already has a name - find it and check if it is the same as given name
        for (const auto & e : m_NamedCaptures) {
            if (e.second == k) {
                if (e.first != name)
                    throw regex_syntax_error("groups with the same index can not have different names");
                break;
            }
        }
    }
    m_NamedCapturesIndeces.insert(k);
    return (m_NamedCaptures[name] = k);
}

int MFA_Builder::getNamedCaptureGroup(const std::string & name, bool defineNew) {
    auto it = m_NamedCaptures.find(name);
    if (it == m_NamedCaptures.end()) {
        // capture group with this name does not exist
        if (defineNew) {
            return defineNamedCaptureGroup(name, nextCGIndex());
        } else {
            throw regex_syntax_error("unknown named capture group '" + name + "'");
        }
    }
    return it->second;
}

SyntaxTreeNode * MFA_Builder::makeNamedBackref(const std::string & name) {
    BackrefNode * node = new BackrefNode(0);
    auto it = m_NamedCaptures.find(name);
    if (it != m_NamedCaptures.end()) {
        // found capture group with this name
        node->m_MemIndex = it->second - 1;
        return node;
    }
    // undefined named capture group, will try to resolve later
    // the memory index will be corrected later (if this named cg is eventually defined and resolved)
    m_UnresolvedCaptures.emplace_back(name, node);
    return node;
}

void MFA_Builder::resolveNamedCaptures() {
    // try to resolve references to previously undefined capture groups
    for (const auto & ref : m_UnresolvedCaptures) {
        auto it = m_NamedCaptures.find(ref.first);
        if (it == m_NamedCaptures.end())
            throw regex_syntax_error("reference to undefined named capture group '" + ref.first + "'");
        else
            ref.second->m_MemIndex = ref.second->m_Tin->m_Transition.m_MemIndex = it->second - 1;
    }
}

} /* namespace mfa */
