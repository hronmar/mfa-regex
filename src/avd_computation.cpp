/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "avd_computation.h"

#include <queue>

int mfa::computeActiveVariables(mfa::MemoryAutomaton * mfa,
    std::unordered_map<mfa::MemoryAutomaton::State *, std::vector<bool>> & activeVariableSets) {

    using State = typename MemoryAutomaton::State;
    using Transition = typename MemoryAutomaton::Transition;

    // first we compute the def relation, and also compute inverse graph 
    // of transitions and recalling states for call relation computation
    std::unordered_map<State *, std::vector<bool>> defRelation;
    std::unordered_map<State *, std::vector<bool>> callRelation;
    std::unordered_map<State *, std::unordered_set<State *>> inverseGraph;
    std::vector<State *> recalls;

    // initialize relation sets as empty
    defRelation.reserve(mfa->m_States.size());
    callRelation.reserve(mfa->m_States.size());
    activeVariableSets.reserve(mfa->m_States.size());
    for (size_t i = 0; i < mfa->m_States.size(); i++) {
        State * st = mfa->m_States.data() + i;
        defRelation[st] = std::vector<bool>(mfa->m_NumMemories, false);
        callRelation[st] = std::vector<bool>(mfa->m_NumMemories, false);
        activeVariableSets[st] = std::vector<bool>(mfa->m_NumMemories, false);
    }

    for (size_t i = 0; i < mfa->m_States.size(); i++) {
        State * st = mfa->m_States.data() + i;

        if (st->m_NextState1 != nullptr) {
            inverseGraph[st->m_NextState1].insert(st);
            if (st->m_NextState2 != nullptr)
                inverseGraph[st->m_NextState2].insert(st);
        }

        if (st->m_Transition.m_Type == Transition::MEMORY_RECALL) {
            recalls.emplace_back(st);
        } else if (st->m_Transition.m_Type == Transition::MEMORY_OPEN) {
            const int memIdx = st->m_Transition.m_MemIndex;
            if (defRelation[st][memIdx]) {
                // already visited this state in previous BFS for this memIdx
                continue;
            }
            // run BFS from st to find states reachable with path containing open(memIdx)
            std::unordered_set<State *> visited;
            std::queue<State *> q;
            q.push(st);
            visited.insert(st);
            while (!q.empty()) {
                State * next = q.front();
                q.pop();
                next->forEachTransition([&q, &visited, memIdx, &defRelation] (const Transition & tr, State * st) {
                    if (visited.count(st) == 0) {
                        visited.insert(st);
                        q.push(st);
                        defRelation[st][memIdx] = true;
                    }
                });
            }
        }
    }

    // now we compute the call relation
    for (State * recallSt : recalls) {
        const int memIdx = recallSt->m_Transition.m_MemIndex;
        if (callRelation[recallSt][memIdx]) {
            // already visited in previous BFS
            continue;
        }
        std::unordered_set<State *> visited;
        std::queue<State *> q;
        q.push(recallSt);
        visited.insert(recallSt);
        callRelation[recallSt][memIdx] = true;
        while (!q.empty()) {
            State * st = q.front();
            q.pop();
            for (State * st2 : inverseGraph[st]) {
                if (visited.count(st2) == 0
                    && (st2->m_Transition.m_Type != Transition::MEMORY_OPEN || st2->m_Transition.m_MemIndex != memIdx)) {
                    visited.insert(st2);
                    q.push(st2);
                    callRelation[st2][memIdx] = true;
                }
            }
        }
    }

    int activeVariableDegree = 0;
    // here we have computed both the relations: def and call,
    // the active variable set is intersection of those sets
    for (size_t i = 0; i < mfa->m_States.size(); i++) {
        State * st = mfa->m_States.data() + i;
        int avsSize = 0;
        for (int memIdx = 0; memIdx < mfa->m_NumMemories; memIdx++)
            if ((activeVariableSets[st][memIdx] = (defRelation[st][memIdx] && callRelation[st][memIdx])))
                avsSize++;
        activeVariableDegree = std::max(activeVariableDegree, avsSize);
    }

    return activeVariableDegree;
}
