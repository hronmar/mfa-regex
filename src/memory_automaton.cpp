/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "memory_automaton.h"
#include "syntax_tree.h"
#include "mfa_builder.h"

namespace mfa {

MemoryAutomaton::MemoryAutomaton(Parameters params) : m_Parameters(params), 
    m_NumMemories(0), m_NumCounters(0), m_InitState(nullptr), m_FinalState(nullptr), 
    m_SyntaxTree(nullptr), m_NumUsedMemories(0) {
}

MemoryAutomaton::MemoryAutomaton(const std::string & pattern, Parameters params) : MemoryAutomaton(params) {
    MFA_Builder builder;
    builder.buildInto(pattern, this);
}

MemoryAutomaton::~MemoryAutomaton() {
    delete m_SyntaxTree;
}

Parameters MemoryAutomaton::getParameters() const {
    return m_Parameters;
}

void MemoryAutomaton::prepareStates(int numStates) {
    m_States.reserve(numStates);
}

MemoryAutomaton::State * MemoryAutomaton::addState() {
    // reallocation will never happen, it would invalidate pointers between states
    assert(m_States.size() < m_States.capacity());
    m_States.emplace_back();
    return &m_States.back();
}

void MemoryAutomaton::addTransition(Transition tr, State * from, State * to, bool lazy) {
    if (lazy)
        from->addLazyTransition(tr, to);
    else
        from->addTransition(tr, to);
    if (tr.isMemoryTransition()) {
        if (tr.m_Type == Transition::MEMORY_RECALL) {
            m_RecalledMemories.insert(tr.m_MemIndex);
        }
        m_NumUsedMemories = std::max(m_NumUsedMemories, tr.m_MemIndex + 1);
    } else if (tr.m_Type == Transition::COUNTER_INC || tr.m_Type == Transition::COUNTER_OUT) {
        m_NumCounters = std::max(m_NumCounters, tr.m_CountingCn.m_CounterIdx + 1);
    }
}

void MemoryAutomaton::addMemory() {
    m_NumMemories++;
}

void MemoryAutomaton::setInitialState(State * state) {
    m_InitState = state;
}

void MemoryAutomaton::setFinalState(State * state) {
    m_FinalState = state;
}

void MemoryAutomaton::finalize() {
    if (m_NumUsedMemories > m_NumMemories) {
        throw automaton_reference_error(m_NumUsedMemories - 1);
    }

    if (m_Parameters.m_UnusedMemoriesOptEn)
        removeUnusedMemories();
}

int MemoryAutomaton::getNumMemories() const {
    return m_NumMemories;
}

void MemoryAutomaton::setSyntaxTree(SyntaxTreeNode * syntaxTree) {
    m_SyntaxTree = syntaxTree;
}

SyntaxTreeNode * MemoryAutomaton::getSyntaxTree() const {
    return m_SyntaxTree;
}

void MemoryAutomaton::setInput(const std::string & str) {
    m_Input = str;
}

void MemoryAutomaton::setInput(std::string && str) {
    m_Input = std::move(str);
}

std::string & MemoryAutomaton::getInput() {
    return m_Input;
}

MemoryAutomaton::Configuration MemoryAutomaton::getInitialConfiguration() {
    return Configuration(m_InitState, StringUtils::makeIterator(m_Input), m_Input, m_NumMemories, m_NumCounters);
}

bool MemoryAutomaton::isAcceptingConfiguration(const Configuration & conf, bool searching) const {
    return (conf.m_State == m_FinalState && (searching || StringUtils::isAtEnd(conf.m_InputIter, m_Input)));
}

std::vector<MemoryAutomaton::Configuration> MemoryAutomaton::getNextConfigurations(const Configuration & conf) const {
    std::vector<Configuration> nextConfs;
    forEachNextConfiguration(conf,
        [&nextConfs](Configuration c) {
            nextConfs.push_back(std::move(c));
        });
    return nextConfs;
}

bool MemoryAutomaton::executeTransition(Configuration & conf, const Transition & tr, State * to) const {
    switch (tr.m_Type) {
        case Transition::EPSILON:
            break;
        case Transition::SYMBOL:
            if (StringUtils::isAtEnd(conf.m_InputIter, m_Input) || tr.m_Symbol != *conf.m_InputIter)
                return false;
            ++conf.m_InputIter;
            break;
        case Transition::DOT:
            if (StringUtils::isAtEnd(conf.m_InputIter, m_Input))
                return false;
            ++conf.m_InputIter;
            break;
        case Transition::CHARACTER_SET:
            if (StringUtils::isAtEnd(conf.m_InputIter, m_Input) || !tr.m_CharacterSet->contains(*conf.m_InputIter))
                return false;
            ++conf.m_InputIter;
            break;
        case Transition::MEMORY_OPEN:
        {
            Memory & mem = conf.m_Memories[tr.m_MemIndex];
            mem.m_Open = true;
            mem.m_Begin = mem.m_End = conf.m_InputIter;
            break;
        }
        case Transition::MEMORY_CLOSE:
        {
            Memory & mem = conf.m_Memories[tr.m_MemIndex];
            mem.m_Open = false;
            mem.m_End = conf.m_InputIter;
            break;
        }
        case Transition::MEMORY_RECALL:
        {
            Memory & mem = conf.m_Memories[tr.m_MemIndex];
            iterator_t inputIter = conf.m_InputIter;
            iterator_t memIter = mem.m_Begin;
            while (memIter != mem.m_End) {
                if (StringUtils::isAtEnd(inputIter, m_Input)
                        || *memIter++ != *inputIter++) {
                    return false;
                }
            }
            conf.m_InputIter = inputIter;
            break;
        }
        case Transition::COUNTER_INC:
        {
            int & count = conf.m_Counters[tr.m_CountingCn.m_CounterIdx];
            if (!tr.m_CountingCn.checkInc(count))
                return false;
            count++;
            if (tr.m_CountingCn.isUnbounded() && count >= tr.m_CountingCn.m_Min) {
                // if UNBOUNDED, stop counting after reaching m_Min to decrease number of configurations
                count = tr.m_CountingCn.m_Min;
            }
            break;
        }
        case Transition::COUNTER_OUT:
            if (!tr.m_CountingCn.check(conf.m_Counters[tr.m_CountingCn.m_CounterIdx]))
                return false;
            conf.m_Counters[tr.m_CountingCn.m_CounterIdx] = 0;
            break;
        case Transition::START_ANCHOR:
            if (!StringUtils::isAtStart(conf.m_InputIter, m_Input))
                return false;
            break;
        case Transition::END_ANCHOR:
            if (!StringUtils::isAtEnd(conf.m_InputIter, m_Input))
                return false;
            break;
        case Transition::WORD_BOUNDARY:
            if (!isAtWordBoundary(conf))
                return false;
            break;
        case Transition::NOT_WORD_BOUNDARY:
            if (isAtWordBoundary(conf))
                return false;
            break;
    }
    conf.m_State = to;
    return true;
}

bool MemoryAutomaton::isAtWordBoundary(const Configuration & conf) const {
    static const CharacterSet word(CharacterSet::forClass("word"));
    if (StringUtils::isAtStart(conf.m_InputIter, m_Input)) {
        if (m_Input.size() == 0)
            return false;
        else
            return word.contains(*conf.m_InputIter);
    } else if (StringUtils::isAtEnd(conf.m_InputIter, m_Input)) {
        iterator_t prev = conf.m_InputIter;
        return word.contains(*--prev);
    } else {
        iterator_t prev = conf.m_InputIter;
        --prev;
        return (word.contains(*conf.m_InputIter) != word.contains(*prev));
    }
}

void MemoryAutomaton::removeUnusedMemories() {
    if (m_RecalledMemories.size() == static_cast<size_t>(m_NumMemories)) {
        // no unreferenced memories
        return;
    }

    // mapping: old memory idx. -> new memory idx.
    constexpr int UNUSED = -1;
    std::vector<int> memMapping(m_NumMemories, UNUSED);
    int j = 0;
    for (int i = 0; i < m_NumMemories; i++) {
        if (m_RecalledMemories.count(i) != 0)
            memMapping[i] = j++;
    }
    m_NumMemories = j;

    for (State & st : m_States) {
        Transition & tr = st.m_Transition;
        if (tr.isMemoryTransition()) {
            tr.m_MemIndex = memMapping[tr.m_MemIndex];
            if (tr.m_MemIndex == UNUSED)
                tr.m_Type = Transition::EPSILON;
        }
    }

    m_NumUsedMemories = m_NumMemories;
}

} /* namespace mfa */
