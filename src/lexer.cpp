/*
 * The MIT License
 *
 * Copyright (c) 2020 Martin Hron
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "lexer.h"
#include "character_set.h"
#include "counting_constraint.h"
#include "exceptions.h"

#include <climits>
#include <stdexcept>

namespace mfa {

const CharacterSet Lexer::SPECIAL_CHARACTERS{false, "|.*+?\\(){}[]^$"};

parser::symbol_type yylex(Lexer * lexer) {
    return lexer->getNext();
}

Lexer::Lexer(const std::string & input) : m_Input(input),
    m_InputIter(StringUtils::makeIterator(m_Input)) {
    readCharacter();
}

parser::symbol_type Lexer::getNext() {

//qInit:
    switch (m_Character) {
        case '|':
            readCharacter();
            return parser::make_VBAR();
        case '.':
            readCharacter();
            return parser::make_DOT();
        case '*':
            readCharacter();
            return parser::make_STAR();
        case '+':
            readCharacter();
            return parser::make_PLUS();
        case '?':
            readCharacter();
            return parser::make_QUESTION_M();
        case '^':
            readCharacter();
            return parser::make_CARET();
        case '$':
            readCharacter();
            return parser::make_DOLLAR();
        case '\\':
            readCharacter();
            goto qEscape;
        case '(':
            readCharacter();
            goto qLPar;
        case ')':
            readCharacter();
            return parser::make_RPAREN();
        case '{':
            readCharacter();
            goto qCounter;
        case '[':
            readCharacter();
            goto qBracketBegin;
        case EOI:
            return parser::make_END();
        default:
        {
            char_t c = m_Character;
            readCharacter();
            return parser::make_SYMBOL(c);
        }
    }

qEscape:
    if (std::isdigit(m_Character) && m_Character != '0') {
         // numbered backreference
        int n = m_Character - '0';
        readCharacter();
        return parser::make_BACKREF(n);
    } else if (m_Character == 'k') {
        // back-reference to named capture group \k<name>
        readCharacter();
        readCharacter('<');
        std::string name;
        readIdentifier(name);
        readCharacter('>');
        return parser::make_NAMED_BACKREF(name);
    } else if (m_Character == EOI) {
        throw regex_syntax_error("unexpected '\\' at the end of pattern");
    } else {
        return handleEscape();
    }

qLPar:
    if (m_Character != '?')
        return parser::make_LPAREN();
    else {
        readCharacter();
        if (m_Character == ':') { // non-capturing parentheses
            readCharacter();
            return parser::make_NONCAP_LPAR();
        } else if (m_Character == '|') { // non-capturing parentheses that resets cg index for each alternation term
            readCharacter();
            return parser::make_NONCAP_LPAR_RESET();
        } else if (m_Character == '<') { // named capture group
            readCharacter();
            std::string ident;
            bool redefinition = false;
            if (m_Character == '&') {
                redefinition = true;
                readCharacter();
            }
            readIdentifier(ident);
            readCharacter('>');
            return redefinition ? parser::make_NAMED_REDEFINITION(ident) : parser::make_NAMED_CAPTURE(ident);
        } else {
            throw regex_syntax_error("unexpected '(?' in pattern");
        }
    }

qCounter:
    {
        int min, max;
        try {
            min = readNumber();
        } catch (const std::overflow_error &) {
            throw regex_syntax_error("number outside of allowed range <0, " + std::to_string(INT_MAX) + ">");
        }
        max = min;
        if (m_Character == ',') {
            readCharacter();
            if (m_Character == '}')
                max = CountingConstraint::UNBOUNDED;
            else {
                try {
                    max = readNumber();
                } catch (const std::overflow_error &) {
                    throw regex_syntax_error("number outside of allowed range <0, " + std::to_string(INT_MAX) + ">");
                }
            }
        }
        if (m_Character != '}')
            throw regex_syntax_error("counting constraint missing terminating '}'");
        readCharacter();
        return parser::make_COUNTER(CountingConstraint(min, max));
    }

qBracketBegin:
    CharacterSet charSet;
    char_t ch;
    std::string charClassName;

    if (m_Character == '^') {
        // if bracket-expression begins with '^', it behaves as complement
        charSet.setComplement(true);
        readCharacter();
    }

    if (m_Character == ']' || m_Character == '-') {
        // these character are treated as literal characters if they are 
        // the first character inside bracket-expression
        ch = m_Character;
        readCharacter();
        goto qBracketEndRange;
    }

qBracketStartRange:
    // single symbol / range start
    switch (m_Character) {
        case ']':
            readCharacter();
            return parser::make_BRACKET_EXPR(charSet);
        case EOI:
            // invalid (non-terminated) bracket-expression
            throw regex_syntax_error("bracket-expression missing terminating ']'");
        case '-':
            readCharacter();
            if (m_Character == ']') {
                // if '-' is the last character in brackets, it represents the symbol '-' being in character set
                charSet.addCharacter('-');
                goto qBracketStartRange; // bracket end handled above
            }
            // invalid bracket-expression, unexpected '-'
            throw regex_syntax_error("invalid bracket-expression, unexpected '-'");
        case '[':
            // character classes, collating symbols, equivalence classes
            readCharacter();
            switch (m_Character) {
                case '.':
                    readCharacter();
                    goto qCollatingElem;
                case '=':
                    readCharacter();
                    goto qEquivClass;
                case ':':
                    readCharacter();
                    charClassName = "";
                    goto qCharClass;
                default:
                    ch = '[';
                    goto qBracketEndRange;
            }
        default:
            ch = m_Character;
            readCharacter();
            goto qBracketEndRange;
    }

qBracketEndRange:
    if (m_Character == '-') {
        // range
        readCharacter();
        switch (m_Character) {
            case ']':
                charSet.addCharacter(ch);
                charSet.addCharacter('-');
                readCharacter();
                return parser::make_BRACKET_EXPR(charSet);
            case EOI:
                goto qBracketStartRange; // unexpected EOI, handled above
            default:
            {
                char end = m_Character;
                charSet.addRange(ch, end);
                readCharacter();
                goto qBracketStartRange;
            }
        }
    } else {
        // just single character
        charSet.addCharacter(ch);
        goto qBracketStartRange;
    }

qCharClass:
    switch (m_Character) {
        case EOI:
            throw regex_syntax_error("unterminated character class inside bracket-expression");
        case ':':
            readCharacter();
            if (m_Character != ']')
                throw regex_syntax_error("invalid character class expression inside bracket-expression");
            try {
                charSet.addCharacterClass(charClassName);
            } catch (character_set_error &) {
                throw regex_syntax_error("unknown character class '" + charClassName + "'");
            }
            readCharacter();
            goto qBracketStartRange;
        default:
            StringUtils::append(m_Character, charClassName);
            readCharacter();
            goto qCharClass;
    }

qCollatingElem:
    throw std::logic_error("collating elem not yet implemented");

qEquivClass:
    throw std::logic_error("equiv class not yet implemented");
}

void Lexer::readCharacter() {
    if (!StringUtils::isAtEnd(m_InputIter, m_Input)) {
        m_Character = *m_InputIter++;
    } else {
        m_Character = EOI; // end of input
    }
}

void Lexer::readCharacter(char_t requestedCharacter) {
    if (m_Character != requestedCharacter)
        throw regex_syntax_error("expected '" + StringUtils::charToString(requestedCharacter) + "'");
    readCharacter();
}

int Lexer::readNumber(int base, int maxLen) {
    int x = 0;
    int len = 0;
    while (len < maxLen) {
        int digitValue;
        if (std::isdigit(m_Character))
            digitValue = m_Character - '0';
        else if (std::isalpha(m_Character) && base > 10)
            digitValue = std::tolower(m_Character) - 'a' + 10;
        else
            break;

        if (digitValue > base)
            break;

        if (x > 0 && base > INT_MAX / x)
            throw std::overflow_error("number outside integer range");
        x *= base;
        if (digitValue > INT_MAX - x)
            throw std::overflow_error("number outside integer range");
        x += digitValue;

        readCharacter();
        len++;
    }
    if (len == 0) {
        throw regex_syntax_error("number expected");
    }
    return x;
}

void Lexer::readIdentifier(std::string & ident) {
    if (!isalpha(m_Character))
        throw regex_syntax_error("invalid identifier");
    do {
        ident.push_back(m_Character);
        readCharacter();
    } while (isalnum(m_Character));
}

parser::symbol_type Lexer::handleEscape() {
    char c = m_Character;
    readCharacter();
    switch (c) {
        case 'a':
            return parser::make_SYMBOL('\a');
        case 'e':
            return parser::make_SYMBOL(0x1B);
        case 'f':
            return parser::make_SYMBOL('\f');
        case 'n':
            return parser::make_SYMBOL('\n');
        case 'r':
            return parser::make_SYMBOL('\r');
        case 't':
            return parser::make_SYMBOL('\t');
        case '0':
            return parser::make_SYMBOL(readNumber(8, 2)); // character by octal code
        case 'o':
        {
            readCharacter('{');
            int x = readNumber(8);
            readCharacter('}');
            return parser::make_SYMBOL(x); // character by octal code
        }
        case 'x':
            if (m_Character == '{') {
                readCharacter('{');
                int x = readNumber(16);
                readCharacter('}');
                return parser::make_SYMBOL(x); // character by hex code
            } else
                return parser::make_SYMBOL(readNumber(16, 2)); // character by hex code
        case 'g':
        {
            int n;
            bool brackets = (m_Character == '{');
            bool relative = false;
            if (brackets)
                readCharacter('{');
            if (m_Character == '-') { // relative backreference
                relative = true;
                readCharacter();
                n = -readNumber();
            } else {
                n = readNumber();
            }
            if (n == 0)
                throw regex_syntax_error("invalid backreference");
            if (brackets)
                readCharacter('}');
            return (relative ? parser::make_RELATIVE_BACKREF(n) : parser::make_BACKREF(n));
        }
        case 'C':
            return parser::make_DOT(); // \C behaves as '.' in this implementation
        case 'A':
            return parser::make_CARET(); // start of subject (input string)
        case 'Z':
            return parser::make_DOLLAR(); // end of subject (input string)
        case 'd':
            return parser::make_BRACKET_EXPR(CharacterSet::forClass("digit"));
        case 'l':
            return parser::make_BRACKET_EXPR(CharacterSet::forClass("lower"));
        case 's':
            return parser::make_BRACKET_EXPR(CharacterSet::forClass("space"));
        case 'u':
            return parser::make_BRACKET_EXPR(CharacterSet::forClass("upper"));
        case 'w':
            return parser::make_BRACKET_EXPR(CharacterSet::forClass("word"));
        case 'D':
            return parser::make_BRACKET_EXPR(CharacterSet::forClass("digit", true));
        case 'L':
            return parser::make_BRACKET_EXPR(CharacterSet::forClass("lower", true));
        case 'S':
            return parser::make_BRACKET_EXPR(CharacterSet::forClass("space", true));
        case 'U':
            return parser::make_BRACKET_EXPR(CharacterSet::forClass("upper", true));
        case 'W':
            return parser::make_BRACKET_EXPR(CharacterSet::forClass("word", true));
        case 'b':
            return parser::make_WORD_BOUNDARY_ASSERT(false);
        case 'B':
            return parser::make_WORD_BOUNDARY_ASSERT(true);
        default:
            if (SPECIAL_CHARACTERS.contains(c))
                return parser::make_SYMBOL(c);
            else
                throw regex_syntax_error("unknown '\\" + StringUtils::charToString(c) + "' escape sequence");
    }
}

} /* namespace mfa */
