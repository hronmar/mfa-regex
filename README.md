# mfa-regex

This is a regular expression library based on memory automata (or MFA for short - hence the name) as introduced in the article ['Characterising REGEX languages by regular languages equipped with factor-referencing'](https://www.sciencedirect.com/science/article/abs/pii/S0890540116000109). This was implemented as part of my master's thesis ['Backreferences in practical regular expressions'](https://dspace.cvut.cz/handle/10467/87858). The implemented matching algorithms are based on the article ['Regular Expressions with Backreferences: Polynomial-Time Matching Techniques'](https://arxiv.org/pdf/1903.05896.pdf) by Markus Schmid. The efficient technique based on active variable degree from the article is implemented.

Matching regular expressions with backreferences is in general NP-complete (see e.g. https://perl.plover.com/NPC/). However, when the number of backreferences (to different capturing groups) is bounded by a constant, the matching can be done in deterministic polynomial time. Furthermore, the technique based on active variables allows to match some patterns in polynomial time even for unlimited number of backreferences, if the pattern satisfies some property (has bounded active variable degree). See the thesis [text](https://dspace.cvut.cz/bitstream/handle/10467/87858/F8-DP-2020-Hron-Martin-thesis.pdf) and also the aforementioned [article](https://arxiv.org/pdf/1903.05896.pdf) for theoretical background.

## Building the library

### Prerequisites
The project uses CMake ([cmake.org](https://cmake.org/)) build generator, CMake version 3.1 or newer is required. To compile you need a C++ compiler with support for C++14 standard. GCC (g++) version 9.3.0 and clang version 10.0 are both confirmed to work, but any C++14 compliant compiler should do.

Additionally, GNU Bison ([www.gnu.org/software/bison/](https://www.gnu.org/software/bison/)) *version at least 3.2* is needed for generating the parser. Finally, the Doxygen ([doxygen.nl](http://doxygen.nl/)) tool is needed for building the documentation. All the above are standard tools and should also be available as packages on most Linux distributions.

### Building and installation

See https://cmake.org/runningcmake/ for instructions on how to use CMake. For example, on Linux the project can be build  by running the following commands from the mfa-regex root directory:

```bash
mkdir build && cd build
cmake ..
make
```

To run the tests, execute:
```bash
make test
```

Additionally, the documentation may be built by running:
```bash
make doc
```

Finally, the library and its utilities can be installed using:
```bash
make install
```

The install path can be chosen during the CMake configuration. Depending on the path, it may be necessary to run the above command as a super user.

## Library overview

The mfa-regex library API consists of two main parts: automaton classes, which represent the compiled regex patterns, and matcher classes, which implement the matching algorithms. A brief overview of the API along with examples is provided here, for further details see relevant pages in the Doxygen generated documentation.

The basic automaton class is *MemoryAutomaton*. Its constructor accepts the pattern from which the automaton will be compiled. Second automaton class (with equivalent interface) is *AvdOptimizedMFA*. This class implements the optimization based on active variable degree (see the previously linked [article](https://arxiv.org/pdf/1903.05896.pdf)), which has some overhead, but may speed up matching on patterns with large number of capturing groups.

Two matching algorithms are provided: *BacktrackMatcher* and *BFSMatcher*. It is strongly recommended to use the former (BacktrackMatcher), especially if information about the match is required. This is because the breadth-first search algorithm will find *any* match, and may not strictly follow the matching semantics (e.g. greedy, lazy quantifiers). The matches (and submatches) reported by backtracking algorithm will always follow the matching semantics: the quantifier variants behave as described in the [Supported syntax](#supported-syntax) section below, when searching, the leftmost match is returned. The two following functions are provided by the matchers:

Function | Description
--- | ---
*match* | Attempts to match an entire input on given automaton.
*search* | Attempts to match any part of input (i.e. performs search) on given automaton.

Both functions return boolean value indicating whether a match was found, and optionally also provide information about the matched substring and contents of the capturing groups. Note that the *match* function returns *true* only if the *whole* input is matched, whereas *search* will report match anywhere inside the input string.

### Example code

To illustrate the library usage, the following simple example code is provided. See also how the utilities in [src/tools](src/tools) folder are implemented for another examples.

```cpp
#include <iostream>
#include <mfa-regex/memory_automaton.h>
#include <mfa-regex/backtrack_matcher.h>

int main() {
    mfa::MemoryAutomaton automaton("(a+)b*\\1"); // create automaton for regex pattern (a+)b*\1
    mfa::MatchResult result; // this will store the match details

    // search for given pattern inside input text "bbaabbaabb" using the backtracking algorithm
    if (mfa::BacktrackMatcher<mfa::MemoryAutomaton>{}.search(&automaton, "bbaabbaabb", &result)) {
        std::cout << "found match of length " << result.getLength()
                  << ", starting on position " << result.getPosition() << std::endl;
        std::cout << "matched string: '" << result.getMatchedString() << "'" << std::endl;
        std::cout << "capture group 1 contents: '" << result[1] << "'" << std::endl;
    } else {
        std::cout << "no match" << std::endl;
    }
    return 0;
}
```

Running this program would produce the following output:

```less
found match of length 6, starting on position 2
matched string: 'aabbaa'
capture group 1 contents: 'aa'
```

Additionally, to use the technique based on active variable degree, simply use AvdOptimizedMFA class instead of MemoryAutomaton in the above code (and also include the `avd_optimized_mfa.h` header).

## Utilities

Two command-line utilities are included with the library. The first one is `mfa-regex-test`, which is an interactive command-line tool to test regular expression patterns on given inputs, it also presents syntax tree of the pattern for better understanding of what is happening.

The second tool is a (rather minimalistic) imitation of the Unix `grep` command, it is run as:
```bash
mfa-grep PATTERN [FILE]...
```

## Supported syntax

The syntax is based on POSIX ERE and PCRE syntax. Each character in pattern matches itself, except the following special characters (metacharacters):

```less
. [ { } ( ) \ * + ? | ^ $
```

To match one of the special characters, prefix it with backslash: for instance `\*` to match '\*' as a literal character.

### Basic syntax

Token | Description
--- | ---
`*` | Matches the preceding element zero or more times (greedy).
`+` | Matches the preceding element one or more times (greedy).
`?` | Matches the preceding element zero or one times (greedy).
`*?` `+?` `??` | Lazy variants of the three preceding constructs (see below for semantics).
`\|` | Alternation, pattern of the form `(a\|b)` matches either the *a* or the *b* subexpression.
`{k}` | Matches the preceding element exactly *k* times.
`{k,l}` | Matches the preceding element at least *k* and at most *l* times.
`{k,}` | Matches the preceding element at least *k* times.
`{ }?` | Lazy variant of any of the preceding counting constraints.
`.` | Matches any single character.
`^` | Matches only at the starting position in the input string.
`$` | Matches only at the ending position of the input string.

By default, all quantifiers behave as *greedy*, they always consume as many input characters as possible. For instance, given input "aaab", the pattern `a+` would match "aaa". In contrast, *lazy* quantifiers match as few characters as possible. For the same example, the pattern `a+?` would match only "a".

### Bracket expressions

Bracket expression is a construct of the form `[...]` and matches any character between the brackets. For instance `[xyz]` matches 'x', 'y', or 'z'. Bracket expressions can also contain character ranges like `[a-z]`, or character classes `[:class_name:]`. The standard C++ localization library *std::locale* is used for character classes support, see https://en.cppreference.com/w/cpp/header/locale for available character classifications. Additionally, `[:word:]` class is supported and has the same meaning as `[[:alnum:]_]`.

Special characters lose their original meaning inside brackets. Symbol `]` stands for itself only if it is the first character in bracket expression. Similarly, `-` is treated as a literal if it is the first or the last character. `^` matches itself anywhere except the first position inside brackets.

Bracket expression that begins with `^` is *negated*, it matches the complement of characters that a bracket expression without the `^` would match. For example, `[^ab]` matches any characters *except* 'a' and 'b'.

### Escape sequences

The following escape sequences have a special meaning. If `\` is not followed by a special character or any of the below escape sequences, the pattern is invalid.

Escape | Description
--- | ---
`\a` | alarm (0x07)
`\e` | escape (0x1B)
`\f` | form feed (0x0C)
`\n` | newline (0x0A)
`\r` | carriage return (0x0D)
`\t` | tab (0x09)
`\Odd` | character with octal code 0dd
`\o{ddd..}` | character with octal code 0ddd..
`\Xdd` | character with hexadecimal code 0xdd
`\X{ddd..}` | character with hexadecimal code 0xddd..
`\C` | any single character (even with Unicode)
`\A` | start of string (like `^`)
`\Z` | end of string (like `$`)
`\d` `\l` `\s` `\u` `\w` | same as `[[:digit:]]` `[[:lower:]]` `[[:space:]]` `[[:upper:]]` `[[:word:]]` respectively
`\D` `\L` `\S` `\U` `\W` | negated variants of the above
`\b` | word boundary, matches between a character matched by `\w` and a character matched by `\W` or vice versa (also at the beginning or at the end of the string if the first/last character is a word character)
`\B` | not a word boundary (see `\b`)

### Grouping and backreferences

Expression | Description
--- | ---
`(...)` | capturing group
`\n` | backreference to *n*-th capturing group
`\gn` `\g{n}` | backreference to *n*-th capturing group
`\g-n` `\g{-n}` | relative reference to *n*-th capturing group before the current position
`(?<name>...)` | named capturing group
`(?<&name>...)` | named capturing group redefinition (see below)
`\k<name>` | backreference to named capturing group
`(?:...)` | non-capturing group
`(?\|...)` | non-capturing group, reset capturing group index for each alternation term of `...`

Capturing groups are numbered based on the order of their left (opening) parentheses in pattern, starting with 1. Non-capturing groups of any type (such as `(?:...)`) do not participate in this numbering. Non-capturing group of the form `(?|...)` changes the numbering for patterns inside it, each alternative is numbered independently starting with the same number. For instance, in `(?|(a)|(b)(c))` both the group `(a)` and `(b)` have number 1, while `(c)` has number 2.

Groups can be referred to by their number, and in case of named groups also by their name. Two groups with different numbers can not be given the same name, in such case redefinition must be used instead.
Capture group content is empty by default, if backreference is reached before the corresponding group definition, it matches an empty string.

Unlike most regex engines, mfa-regex supports named capturing group *redefinitions*. Group of the form `(?<&name>...)` has the effect that the content of the group with given name is redefined. For example, the pattern `(?<x>a*)\k<x>(?<&x>b+)\k<x>` first records string matched by `a*` into group named 'x', matches it again, and then it records string matched by `b+` into the same group. Groups of the form `(?<&name>...)` are not numbered unless there is no preceding `(?<name>...)`, in which case the first `(?<&name>...)` acts as a named capturing group `(?<name>...)` and is numbered accordingly. For example, the pattern `(?:(?<&x>a)|(?<&x>b))\k<x>` is the same as `(?:(?<x>a)|(?<&x>b))\k<x>` and contains one capture group with name 'x' and number 1. Another alternative pattern to this can be constructed using the resetting non-capture group as `(?|(<x>a)|(<x>b))\k<x>`, but capturing groups redefinitions allow to create even more complex patterns. Using both named and numbered backreferences to reference named capturing group is possible but discouraged, since it can lead to confusing patterns.
