cmake_minimum_required(VERSION 3.1)

project(mfa-regex)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -Wall -pedantic")

option(UNICODE_SUPPORT "Unicode support" ON)
option(BUILD_STATIC_LIBRARY "Build static library" ON)
option(BUILD_DOC "Doxygen documentation" ON)
option(BUILD_TESTS "Build tests" ON)

include(CMakeDependentOption)
cmake_dependent_option(RUN_TESTS "Run the tests after build" OFF "BUILD_TESTS" OFF)

configure_file(config.h.in config.h)

file(GLOB core_sources src/*.cpp)

find_package(BISON REQUIRED)
add_custom_command(
    OUTPUT
        ${CMAKE_CURRENT_BINARY_DIR}/parser.h
        ${CMAKE_CURRENT_BINARY_DIR}/parser.cpp
    COMMAND bison -d ${CMAKE_CURRENT_SOURCE_DIR}/src/parser.yy
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/src/parser.yy
)

include_directories("${CMAKE_CURRENT_SOURCE_DIR}/include"
    "${CMAKE_CURRENT_BINARY_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}/lib")

add_library(mfa-regex SHARED
    parser.h parser.cpp
    ${core_sources}
)

if(BUILD_STATIC_LIBRARY)
    add_library(mfa-regex-static STATIC
        parser.h parser.cpp
        ${core_sources}
    )
endif()

add_executable(mfa-regex-test src/tools/mfa-regex-test.cpp)
add_executable(mfa-grep src/tools/mfa-grep.cpp)
if(BUILD_STATIC_LIBRARY)
    target_link_libraries(mfa-regex-test mfa-regex-static)
    target_link_libraries(mfa-grep mfa-regex-static)
else()
    target_link_libraries(mfa-regex-test mfa-regex)
    target_link_libraries(mfa-grep mfa-regex)
endif()

if(BUILD_DOC)
    find_package(Doxygen)
    if(DOXYGEN_FOUND)
        configure_file(${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)
        add_custom_target(doc
            ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
            COMMENT "Generating documentation with Doxygen" VERBATIM
        )
    else(DOXYGEN_FOUND)
        message("Doxygen is required to generate the documentation")
    endif(DOXYGEN_FOUND)
endif()

if(BUILD_TESTS)
    file(GLOB tests_sources tests/*.cpp)
    add_executable(tests-runner ${tests_sources})
    if(BUILD_STATIC_LIBRARY)
        target_link_libraries(tests-runner mfa-regex-static)
    else()
        target_link_libraries(tests-runner mfa-regex)
    endif()

    add_custom_target(test
        COMMAND tests-runner
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Run tests"
        SOURCES ${tests_sources}
    )

    if(RUN_TESTS)
        add_custom_target(run-tests ALL
            DEPENDS test
        )
    endif()
endif()

install(TARGETS mfa-regex DESTINATION lib/)
if(BUILD_STATIC_LIBRARY)
    install(TARGETS mfa-regex-static DESTINATION lib/)
endif()
file(GLOB HEADERS include/*.h)
install(FILES ${HEADERS} "${CMAKE_CURRENT_BINARY_DIR}/config.h" DESTINATION include/${PROJECT_NAME})
if(UNICODE_SUPPORT)
    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lib/utfcpp/ DESTINATION include/utfcpp/)
endif()

if(BUILD_DOC)
    install(CODE "execute_process(COMMAND \"${CMAKE_COMMAND}\" --build \"${CMAKE_CURRENT_BINARY_DIR}\" --target doc)")
    install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/doc/html DESTINATION doc/${PROJECT_NAME})
endif()

install(TARGETS mfa-regex-test DESTINATION bin)
install(TARGETS mfa-grep DESTINATION bin)
